#!/bin/bash
# IGR
# 16/07/26

# Build bowtie2 indexes of human, chimp and crab-eating macaque genomes for read mapping and alignment. Should really try Kallisto, but no time this time. 
echo "bowtie2-build -f --threads 24 ~/orthoexon/macFas5/macFas5.fa ~/reference/bowtie2_indexes/macFas5/macFas5_all" | qsub -l nodes=1,walltime=24:00:00 -V -o /home/users/ntu/igr/logs/index_macFas5.log -N index_macFas5 -M igr@ntu.edu.sg -m ae -oe

echo "bowtie2-build -f --threads 24 ~/orthoexon/hg38/hg38_renamed_no_alt_analysis_set.fa ~/reference/bowtie2_indexes/hg38/hg38_no_alt" | qsub -l nodes=1,walltime=24:00:00 -V -o /home/users/ntu/igr/logs/index_hg38.log -N index_hg38 -M igr@ntu.edu.sg -m ae -oe

#echo "blat -noHead ~/orthoexon/hg38/hg38_renamed_no_alt_analysis_set.fa ~/orthoexon/exon_hg38/hg38-5_all_exons_ensembl_84_160531.fa${i}.fa ~/orthoexon/exon_hg38/blat_output/hg38_to_hg38/hg38_${i}_blat.out" | qsub -l mem=4gb,walltime=24:00:00 -V -o ~/logs/blat_hg_hg_$i.log -N blat_hg_hg_$i -M igr@ntu.edu.sg -m ae -oe



