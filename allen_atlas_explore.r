7### IGR 2017.03.01
### allen_atlas_explore.r
### For digging through the Allen Brain Atlas human and macaque microarray datasets and pulling out interesting regions. 


############################
### 0. SETUP WORK SPACE. ###
############################

setwd("~/cloudstor/Data/tomoki/brain_atlas_arrays/")
library(gdata)
library(ggplot2)
library(reshape2)
# library(affy)
library(grid)
library(gridExtra)
library(plyr)
library(dplyr)
library(limma)
library(tidyr)
library(preprocessCore)
library(mixOmics)

sessionInfo()

####################################################################
### 1. WRANGLING THE META DATA, IDENTIFYING SAMPLES TO KEEP ETC. ###
####################################################################

allMeta <- read.csv("combined_developmental_metadata.csv", header=T, fill=T) # allMeta is made from manual merging of human and mac meta data.
dim(allMeta)

# Pull comparable data sets:
# From T.Otani: "we could use two comparable time points: E70/PCX16 and E80/PCW21 (sorry it is a bit different from the previous one I suggested to you)"
m70vh16 <- allMeta[allMeta$donor.age == "E70" | allMeta$donor.age == "16pcw",]
m80vh21 <- allMeta[allMeta$donor.age == "E80" | allMeta$donor.age == "21pcw",]

# Regions to keep:
# Identified by T. Otani from a spreadsheet listing all possible brain slices:
macCortex <- c("inner subventricular zone of S1",
"inner subventricular zone of V1",
"inner subventricular zone of rostral cingulate cortex",
"inner ventricular zone of S1",
"inner ventricular zone of V1",
"inner ventricular zone of rostral cingulate cortex",
"outer subventricular zone of S1",
"outer subventricular zone of V1",
"outer subventricular zone of rostral cingulate cortex",
"outer ventricular zone of S1",
"outer ventricular zone of V1",
"outer ventricular zone of rostral cingulate cortex",
"ventricular zone of S1",
"ventricular zone of V1",
"ventricular zone of rostral cingulate cortex")

humanCortex <- c("SZ in rostral cingulate cortex",
    "VZ in primary somatosensory cortex",
    "VZ in primary visual cortex",
    "VZ in rostral cingulate neocortex",
    "inner SZ in primary somatosensory cortex",
    "inner SZ in primary visual cortex",
    "outer SZ in primary somatosensory cortex",
    "outer SZ in primary visual cortex")

m80vh21Cortex <- m80vh21[m80vh21$structure.name %in% c(macCortex, humanCortex),]
m70vh16Cortex <- m70vh16[m70vh16$structure.name %in% c(macCortex, humanCortex),]

m70vh16Cortex <- drop.levels(m70vh16Cortex)
m80vh21Cortex <- drop.levels(m80vh21Cortex)

# Not that many samples!
table(m80vh21Cortex$donor.name)
# H376.IV.02 H376.IV.03   MMU34624   MMU35058   MMU36435   MMU37852 
#          9          5          6          9          9          9 

table(m70vh16Cortex$donor.name)
    # DAM35650 H376.IIIB.02     MMU32143     MMU33757     MMU35475 
    #       12            9           13           12           11 

# And fairly skewed representation of those that do exist!

table(m80vh21Cortex$structure.name)
# inner subventricular zone of rostral cingulate cortex                       inner subventricular zone of S1                       inner subventricular zone of V1 
#                                                     4                                                     4                                                     4 
#              inner SZ in primary somatosensory cortex                     inner SZ in primary visual cortex outer subventricular zone of rostral cingulate cortex 
#                                                     2                                                     1                                                     4 
#                       outer subventricular zone of S1                       outer subventricular zone of V1              outer SZ in primary somatosensory cortex 
#                                                     4                                                     4                                                     2 
#                     outer SZ in primary visual cortex                        SZ in rostral cingulate cortex          ventricular zone of rostral cingulate cortex 
#                                                     1                                                     2                                                     4 
#                                ventricular zone of S1                                ventricular zone of V1                    VZ in primary somatosensory cortex 
#                                                     4                                                     4                                                     3 
#                           VZ in primary visual cortex                     VZ in rostral cingulate neocortex 
#                                                     1                                                     2 

table(m70vh16Cortex$structure.name)
# inner subventricular zone of rostral cingulate cortex                       inner subventricular zone of S1                       inner subventricular zone of V1 
#                                                     4                                                     3                                                     5 
#              inner SZ in primary somatosensory cortex                     inner SZ in primary visual cortex    inner ventricular zone of rostral cingulate cortex 
#                                                     1                                                     1                                                     4 
#                          inner ventricular zone of S1                          inner ventricular zone of V1 outer subventricular zone of rostral cingulate cortex 
#                                                     4                                                     4                                                     4 
#                       outer subventricular zone of S1                       outer subventricular zone of V1              outer SZ in primary somatosensory cortex 
#                                                     4                                                     4                                                     2 
#                     outer SZ in primary visual cortex    outer ventricular zone of rostral cingulate cortex                          outer ventricular zone of S1 
#                                                     1                                                     5                                                     3 
#                          outer ventricular zone of V1                        SZ in rostral cingulate cortex                    VZ in primary somatosensory cortex 
#                                                     4                                                     1                                                     1 
#                           VZ in primary visual cortex                     VZ in rostral cingulate neocortex 
#                                                     1                                                     1 

# So this is what we're interested in looking at - each of these slices. Now it's time to read in the real data and play with it. 

############################################
### 2. PREPROCESS AND FILTER ARRAY DATA. ###
############################################

macArray <- read.csv("lmd_expression_matrix_2014-03-06_macaque/expression_matrix.csv", header=F)
macCols <- read.csv("lmd_expression_matrix_2014-03-06_macaque/columns_metadata.csv", header=T)
macRows <- read.csv("lmd_expression_matrix_2014-03-06_macaque/rows_metadata.csv", header=T)

hum16pcw <- read.csv("lmd_matrix_14751_h376.iiib.02_16pcw/expression_matrix.csv", header=F)
hum16pcwCols <- read.csv("lmd_matrix_14751_h376.iiib.02_16pcw/columns_metadata.csv", header=T)
hum16pcwRows <- read.csv("lmd_matrix_14751_h376.iiib.02_16pcw/rows_metadata.csv", header=T)

hum21pcwA <- read.csv("lmd_matrix_12566_h376.iv.02_21pcw/expression_matrix.csv", header=F)
hum21pcwACols <- read.csv("lmd_matrix_12566_h376.iv.02_21pcw/columns_metadata.csv", header=T)
hum21pcwARows <- read.csv("lmd_matrix_12566_h376.iv.02_21pcw/rows_metadata.csv", header=T)

hum21pcwB <- read.csv("lmd_matrix_12690_h376.iv.03_21pcw/expression_matrix.csv", header=F)
hum21pcwBCols <- read.csv("lmd_matrix_12690_h376.iv.03_21pcw/columns_metadata.csv", header=T)
hum21pcwBRows <- read.csv("lmd_matrix_12690_h376.iv.03_21pcw/rows_metadata.csv", header=T)

hum15pcw <- read.csv("lmd_matrix_12840.h376.iiia.02_15pcw/expression_matrix.csv", header=F)
hum15pcwCols <- read.csv("lmd_matrix_12840.h376.iiia.02_15pcw/columns_metadata.csv", header=T)
hum15pcwRows <- read.csv("lmd_matrix_12840.h376.iiia.02_15pcw/rows_metadata.csv", header=T)

# The humans come in three separate files, so we want to make sure it's all one big compatible file... meaning we can use hum15pcwRows always downstream.  The same is of course not true for the column files
# all(hum16pcwRows$probeset_id == hum15pcwRows$probeset_id)
# [1] TRUE
# all(hum21pcwARows$probeset_id == hum15pcwRows$probeset_id)
# [1] TRUE
# all(hum21pcwBRows$probeset_id == hum15pcwRows$probeset_id)
# [1] TRUE

# From the file description, the first column of all arrays is actually the probe ID, which should be kept because it is not identical to the index... but that means every time I grab data by column I'll be off by 1, because this extra column is not reflected in macCols or equivalent! Hence the + 1 everywhere

# We don't actually need to handle all the arrays separately, since we're gonna bring them together later. 
hum15 <- hum15pcw[,which(hum15pcwCols$structure_name %in% unique(m70vh16Cortex$structure.name))+1]
hum16 <- hum16pcw[,which(hum16pcwCols$structure_name %in% unique(m70vh16Cortex$structure.name))+1]
hum21a <- hum21pcwA[,which(hum21pcwACols$structure_name %in% unique(m80vh21Cortex$structure.name)) + 1]
hum21b <- hum21pcwB[,which(hum21pcwBCols$structure_name %in% unique(m80vh21Cortex$structure.name)) + 1]

names(hum15) <- paste("h15", names(hum15), sep=".")
names(hum16) <- paste("h16", names(hum16), sep=".")
names(hum21a) <- paste("h21a", names(hum21a), sep=".")
names(hum21b) <- paste("h21b", names(hum21b), sep=".")

allHumFilt <- data.frame(hum15, hum16, hum21a, hum21b)
allMacFilt <- macArray[,macCols[which(macCols$structure_name %in% macCortex & macCols$age %in% c("E40", "E50", "E70", "E80", "E90", "E120")),]$column_num + 1]

#Remove that failed column:
names(allMacFilt) <- paste(macCols[which(macCols$structure_name %in% macCortex),]$age, names(allMacFilt), sep=".")
allMacFilt <- allMacFilt[,colSums(is.na(allMacFilt)) == 0]

# How do all five distributions look before normalisation?
    allFiltEarly <- melt(list(allHumFilt, allMacFilt))
    pdf(file="all_filtered_samples_intensities.pdf")
        ggplot(allFiltEarly, aes(value, ..density.., colour=as.factor(variable))) + geom_freqpoly(binwidth=0.1) + scale_x_continuous(name="Normalised intensity") + theme_bw() + guides(colour=F) 
    dev.off()


#############################################################
### 3. READ IN PROBE FILTERING LISTS AND CLEAN UP THINGS. ###
#############################################################

macProbes <- read.table("bakken_et_al_2016_probes.csv", header=T, sep=",")
humProbes <- read.table("miller_et_al_2014_probes.csv", header=T, sep=",")

# New filtering - in macaques can be done in one line, in humans we need a bit more effort to generate that line:
macRows2 <- dplyr::inner_join(macRows, macProbes, by=c("probe_name" = "probeid"))

cleanHumans <- function(x) {
        try(x[which.max(x$R),]$keep_for_analysis <- TRUE, silent=T)
        return(x)
        }

humProbes$keep_for_analysis <- FALSE
humRows <- humProbes %>% group_by(Gene) %>% do(cleanHumans(.))
humRows <- as.data.frame(humRows)
humRows$keep_for_analysis <- ifelse(humRows$pass == FALSE, FALSE, humRows$keep_for_analysis) # to catch the terrible ones
    # question worth asking - do you want a threshold here? R must be greater than x? It would be nice to keep it consistent between the two data sets...

# We join afterwards to ensure order is maintained.  
humRows2 <- dplyr::inner_join(hum15pcwRows, humRows, by=c("probeset_name" = "Probe"))

# Adding metadata, pt1:
allHumFilt <- data.frame(allHumFilt, humRows2[,2])
allMacFilt <- data.frame(allMacFilt, macRows2[,3])

names(allHumFilt)[ncol(allHumFilt)] <- "probeset_name"
names(allMacFilt)[ncol(allMacFilt)] <- "probe_name"

# Filtering only to good probes from overlapping genes. There's multiple probes per gene, so we gotta clean up first, otherwise the inner join explodes
humClean <- humRows2[which(humRows2$keep_for_analysis == T),]
macClean <- macRows2[which(macRows2$keep_for_analysis == T),]

bothClean <- dplyr::inner_join(humClean[,c(1:6,8,11,20)], macClean[,c(2:3,6:10,15:16)], by=c("entrez_id" = "human_entrezid"), suffix=c(".hum", ".mac"))
bothClean2 <- bothClean[bothClean$entrez_id %in% names(which(table(bothClean$entrez_id) == 1)),]

allHumFiltProbes <- allHumFilt[allHumFilt$probeset_name %in% bothClean2$probeset_name,]
allMacFiltProbes <- allMacFilt[allMacFilt$probe_name %in% bothClean2$probe_name,]

# But they are still in different orders and have discordant metadata, so we gotta fix that too before we can merge and quantile norm
allFiltProbes <- dplyr::inner_join(bothClean2, allHumFiltProbes)
allFiltProbes <- dplyr::inner_join(allFiltProbes, allMacFiltProbes)


###############################################
### 4. EXPLORATORY PLOTS AND NORMALISATION. ###
###############################################

# a quick metadata df:
    samplesMeta <- data.frame("columns" = names(allFiltProbes[grepl("\\.V", names(allFiltProbes))]))
    samplesMeta <- cbind(samplesMeta, separate(samplesMeta, "columns", into=c("age", "var")))
    samplesMeta$species <- factor(ifelse(grepl("^h", samplesMeta$columns), "Hs", "Mf"))

    # The numbering is off... again... so we gotta fix it again
    samplesMeta$structure <- ifelse(samplesMeta$species == "Mf", as.character(macCols[as.numeric(gsub("V", "", samplesMeta$var))-1,]$structure_name), "cortex")
    samplesMeta$acronym <- ifelse(samplesMeta$species == "Mf", as.character(macCols[as.numeric(gsub("V", "", samplesMeta$var))-1,]$structure_acronym), "cortex")

    # Humans are messier, as ever. 
    samplesMeta$structure <- ifelse(samplesMeta$species == "Hs" & samplesMeta$age == "h15", as.character(hum15pcwCols[as.numeric(gsub("V", "", samplesMeta$var))-1,]$structure_name), samplesMeta$structure)
    samplesMeta$structure <- ifelse(samplesMeta$species == "Hs" & samplesMeta$age == "h16", as.character(hum16pcwCols[as.numeric(gsub("V", "", samplesMeta$var))-1,]$structure_name), samplesMeta$structure)
    samplesMeta$structure <- ifelse(samplesMeta$species == "Hs" & samplesMeta$age == "h21a", as.character(hum21pcwACols[as.numeric(gsub("V", "", samplesMeta$var))-1,]$structure_name), samplesMeta$structure)
    samplesMeta$structure <- ifelse(samplesMeta$species == "Hs" & samplesMeta$age == "h21b", as.character(hum21pcwBCols[as.numeric(gsub("V", "", samplesMeta$var))-1,]$structure_name), samplesMeta$structure)

    samplesMeta$acronym <- ifelse(samplesMeta$species == "Hs" & samplesMeta$age == "h15", as.character(hum15pcwCols[as.numeric(gsub("V", "", samplesMeta$var))-1,]$structure_acronym), samplesMeta$acronym)
    samplesMeta$acronym <- ifelse(samplesMeta$species == "Hs" & samplesMeta$age == "h16", as.character(hum16pcwCols[as.numeric(gsub("V", "", samplesMeta$var))-1,]$structure_acronym), samplesMeta$acronym)
    samplesMeta$acronym <- ifelse(samplesMeta$species == "Hs" & samplesMeta$age == "h21a", as.character(hum21pcwACols[as.numeric(gsub("V", "", samplesMeta$var))-1,]$structure_acronym), samplesMeta$acronym)
    samplesMeta$acronym <- ifelse(samplesMeta$species == "Hs" & samplesMeta$age == "h21b", as.character(hum21pcwBCols[as.numeric(gsub("V", "", samplesMeta$var))-1,]$structure_acronym), samplesMeta$acronym)

    samplesMeta$age <- gsub("21[[:lower:]]", "21", samplesMeta$age)
    samplesMeta$age <- factor(samplesMeta$age, levels=c("h15", "h16", "h21", "E40", "E50", "E70", "E80", "E90", "E120"))
    samplesMeta$plotAge <- ifelse(samplesMeta$species == "Mf", as.numeric(samplesMeta$age) - 3, as.numeric(samplesMeta$age))

    samplesMeta$structure2 <- gsub("inner |outer ", "", samplesMeta$structure)
    samplesMeta$shape <- as.factor(ifelse(grepl("inner", samplesMeta$structure), "Inner", ifelse(grepl("outer", samplesMeta$structure), "Outer", "Whole")))


# How does it look now? (still terrible!)
ggallFiltProbes <- melt(allFiltProbes[,18:210])
pdf(file="all_filtered_samples_probes_intensities.pdf")
    ggplot(ggallFiltProbes, aes(value, ..density.., colour=as.factor(variable))) + geom_freqpoly(binwidth=0.1) + scale_x_continuous(name="Normalised intensity") + theme_bw() + guides(colour=F) 
dev.off()

# So now let's do this properly:
allFiltProbesNorm <- normalize.quantiles(as.matrix(allFiltProbes[,18:210]), copy=T) # Should have no reference arrays
ggallFiltProbesNorm <- melt(allFiltProbesNorm)
pdf(file="all_filtered_samples_probes_intensities_quantile.pdf")
    ggplot(ggallFiltProbesNorm, aes(value, ..density.., colour=as.factor(Var2))) + geom_freqpoly(binwidth=0.1) + scale_x_continuous(name="Normalised intensity") + theme_bw() + guides(colour=F) 
dev.off()

# Well, that worked - now bring back the metadata. This bit looks super dubious... but is the easiest way of preserving the right column names
allFiltProbesFinal <- allFiltProbes
allFiltProbesFinal[,18:210] <- allFiltProbesNorm

# saveRDS(allFiltProbesFinal, file="all_filt_probes_final.RDS")
# saveRDS(samplesMeta, file="allen_samples_meta.RDS")

#####################################
### 5. OUR FAVOURITE GENE: CENPJ. ###
#####################################

# allFiltProbesFinal <- readRDS(file="all_filt_probes_final.RDS")
# allenSamplesMeta <- readRDS(file="allen_samples_meta.RDS")

cenpj <- allFiltProbesFinal[allFiltProbesFinal$gene_symbol %in% "CENPJ",]

tcenpj <- t(cenpj[,18:210])
tcenpj <- cbind(tcenpj, samplesMeta)
names(tcenpj)[1] <- "int"

# It's not pretty, but it still looks different between the two species, thank god.  
pdf(file="cenpj_all_samples_quant_norm.pdf", width=18)

ggplot(tcenpj, aes(y=int, x=as.numeric(age), colour=structure)) +
    geom_point(size=2) +
    stat_smooth(aes(y=int,x=as.numeric(age))) +
    facet_grid( species ~ structure, drop=T, scales="free_x") + 
    theme_bw() + 
    theme(legend.position="n", axis.text.x = element_text(angle = 45, hjust = 0.5, vjust=0.3), panel.grid.minor = element_blank()) +
    labs(y="Z-transformed probe intensity", title=paste0("CENPJ best probe, (Hs: ", cenpj$probeset_name, ", R^2 = ", cenpj$R, "; Mf: ", cenpj$probe_name, ", R^2 = ", signif(cenpj$affy_rnaseq_cor, 3), ")"), x="Age") +
    scale_x_continuous(breaks = 1:9, labels = c("15PCW", "16PCW" ,"21PCW", levels(tcenpj$age)[4:9]))
dev.off()

# And now collapsing structures
pdf(file="cenpj_all_samples_quant_norm_structures.pdf", width=18)
    ggplot(tcenpj, aes(y=int, x=as.numeric(age), colour=structure2)) +
        geom_point(size=2) +
        geom_smooth(aes(x=as.numeric(age))) +
        facet_grid( species ~ structure2, drop=T, scales="free_x", space="fixed") + 
        theme_bw() + 
        theme(legend.position="n", axis.text.x = element_text(angle = 45, hjust = 0.5, vjust=0.3), panel.grid.minor = element_blank()) +
        labs(y="Z-transformed probe intensity", title=paste0("CENPJ best probe, (Hs: ", cenpj$probeset_name, ", R^2 = ", cenpj$R, "; Mf: ", cenpj$probe_name, ", R^2 = ", signif(cenpj$affy_rnaseq_cor, 3), ")"), x="Age") +
        scale_x_continuous(breaks = 1:9, labels = c("15PCW", "16PCW" ,"21PCW", levels(tcenpj$age)[4:9]))
dev.off()


###############################################
### 4. MACAQUE TIME COURSE (Z-TRANSFORMED). ###
###############################################

macAllFilt <- macArray[,macCols[which(macCols$structure_name %in% macCortex),]$column_num + 1]
macAllFilt2 <- macAllFilt[, colSums(is.na(macAllFilt)) == 0]
macAllFail <- which(colSums(is.na(macAllFilt)) > 0)
macAllRank <- data.frame(apply(macAllFilt2, 2, function(x) qqnorm(x, plot.it=F)))
macAllRankCenpj <- macAllRank[which(macRows$gene_symbol %in% "CENPJ"), grepl("x", colnames(macAllRank))]
macCortexKeep <- macCols[which(macCols$structure_name %in% macCortex),]

tmacAllRankCenpj <- t(macAllRankCenpj)ma
tmacAllRankCenpj <- cbind(tmacAllRankCenpj, macCortexKeep[-macAllFail,])

macRankCenpj <- melt(tmacAllRankCenpj, measure.vars = as.character(which(macRows$gene_symbol %in% "CENPJ")))
macRankCenpj <- drop.levels(macRankCenpj)

macRankCenpj$probe <- macRankCenpj$variable
macRankCenpj$probe <- gsub("10581", "real_probe_name", macRankCenpj$probe)

# Reorder factors:
macRankCenpj$age <- factor(macRankCenpj$age, c("E40", "E50", "E70", "E80", "E90", "E120"))
probe2rank <- macRankCenpj[macRankCenpj$variable == 10581,]

# Combine dot and line plot by structure
# However, many of the categories in the data are not really well designed and should be combined. 
# Remove all mention of inner and outer from plotting variable, but add a column capturing the structure origin:
probe2rank$structure_name2 <- gsub("inner |outer ", "", probe2rank$structure_name)
probe2rank$shape <- as.factor(ifelse(grepl("inner", probe2rank$structure_name), "Inner", ifelse(grepl("outer", probe2rank$structure_name), "Outer", "Whole")))

probe2no120 <- probe2rank[!which(probe2rank$age %in% "E120"),]
probe2no120 <- drop.levels(probe2no120)

pdf("macaque_qnorm_best_CENPJ_probe.pdf", width=15, height=4)
ggplot(probe2rank, aes(y=value, x=age, colour=structure_name)) + geom_point(size=2) + stat_smooth(aes(y=value,x=as.numeric(age))) +  facet_grid( . ~ structure_name) + theme_bw() + theme(legend.position="n") + labs(y="Z-transformed probe intensity", title="CENPJ best probe in macaque (MmugDNA.17253.1.S1_at), LOESS") + ylim(-2,2) + theme(axis.text.x = element_text(angle = 45, hjust = 1))
ggplot(probe2rank, aes(y=value, x=age, colour=structure_name)) + geom_point(size=2) + stat_smooth(aes(y=value,x=as.numeric(age)), method="lm") +  facet_grid( . ~ structure_name) + theme_bw() + theme(legend.position="n") + labs(y="Z-transformed probe intensity", title="CENPJ best probe in macaque (MmugDNA.17253.1.S1_at), LM") + ylim(-2,2) + theme(axis.text.x = element_text(angle = 45, hjust = 1))

ggplot(probe2rank, aes(y=value, x=age, colour=structure_name2)) + geom_point(aes(shape=shape), size=2) + stat_smooth(aes(y=value,x=as.numeric(age))) +  facet_grid( . ~ structure_name2) + theme_bw() + guides(colour=FALSE, shape=guide_legend(title=NULL)) + labs(y="Z-transformed probe intensity", title="CENPJ best probe in macaque (MmugDNA.17253.1.S1_at), LOESS") + ylim(-2,2) + theme(axis.text.x = element_text(angle = 45, hjust = 1))

ggplot(probe2rank, aes(y=value, x=age, colour=structure_name2)) + geom_point(aes(shape=shape), size=2) + stat_smooth(aes(y=value,x=as.numeric(age)), method="lm") +  facet_grid( . ~ structure_name2) + theme_bw() + guides(colour=FALSE, shape=guide_legend(title=NULL)) + labs(y="Z-transformed probe intensity", title="CENPJ best probe in macaque (MmugDNA.17253.1.S1_at), LM") + ylim(-2,2) + theme(axis.text.x = element_text(angle = 45, hjust = 1))
dev.off()

pdf("macaque_qnorm_best_CENPJ_probe_no_E120.pdf", width=15, height=4)
ggplot(probe2no120, aes(y=value, x=age, colour=structure_name)) + geom_point(size=2) + stat_smooth(aes(y=value,x=as.numeric(age))) +  facet_grid( . ~ structure_name) + theme_bw() + theme(legend.position="n") + labs(y="Z-transformed probe intensity", title="CENPJ best probe in macaque (MmugDNA.17253.1.S1_at), LOESS") + ylim(-2,2) + theme(axis.text.x = element_text(angle = 45, hjust = 1))
ggplot(probe2no120, aes(y=value, x=age, colour=structure_name)) + geom_point(size=2) + stat_smooth(aes(y=value,x=as.numeric(age)), method="lm") +  facet_grid( . ~ structure_name) + theme_bw() + theme(legend.position="n") + labs(y="Z-transformed probe intensity", title="CENPJ best probe in macaque (MmugDNA.17253.1.S1_at), LM") + ylim(-2,2) + theme(axis.text.x = element_text(angle = 45, hjust = 1))

ggplot(probe2no120, aes(y=value, x=age, colour=structure_name2)) + geom_point(aes(shape=shape), size=2) + stat_smooth(aes(y=value,x=as.numeric(age))) +  facet_grid( . ~ structure_name2) + theme_bw() + guides(colour=FALSE, shape=guide_legend(title=NULL)) + labs(y="Z-transformed probe intensity", title="CENPJ best probe in macaque (MmugDNA.17253.1.S1_at), LOESS") + ylim(-2,2) + theme(axis.text.x = element_text(angle = 45, hjust = 1))

ggplot(probe2no120, aes(y=value, x=age, colour=structure_name2)) + geom_point(aes(shape=shape), size=2) + stat_smooth(aes(y=value,x=as.numeric(age)), method="lm") +  facet_grid( . ~ structure_name2) + theme_bw() + guides(colour=FALSE, shape=guide_legend(title=NULL)) + labs(y="Z-transformed probe intensity", title="CENPJ best probe in macaque (MmugDNA.17253.1.S1_at), LM") + ylim(-2,2) + theme(axis.text.x = element_text(angle = 45, hjust = 1))
dev.off()



#############################################
### 5. HUMAN TIME COURSE (Z-TRANSFORMED). ###
#############################################



hum15pcwRank <- data.frame(apply(hum15pcwFilt, 2, function(x) qqnorm(x, plot.it=F)))
hum16pcwRank <- data.frame(apply(hum16pcwFilt, 2, function(x) qqnorm(x, plot.it=F)))
hum21pcwRank <- data.frame(apply(hum21pcwFilt, 2, function(x) qqnorm(x, plot.it=F)))

hum15pcwCenpj <- hum15pcwRank[which(hum15pcwRows$gene_symbol %in% "CENPJ"), grepl("x", colnames(hum15pcwRank))]
hum16pcwCenpj <- hum16pcwRank[which(hum16pcwRows$gene_symbol %in% "CENPJ"), grepl("x", colnames(hum16pcwRank))]
hum21pcwCenpj <- hum21pcwRank[which(hum21pcwARows$gene_symbol %in% "CENPJ"), grepl("x", colnames(hum21pcwRank))] # Can use A or B, totally irrelevant

hum15meta <- hum15pcwCols[which(hum15pcwCols$structure_name %in% humanCortex),]
hum16meta <- hum16pcwCols[which(hum16pcwCols$structure_name %in% humanCortex),]
hum21meta <- rbind(hum21pcwACols[which(hum21pcwACols$structure_name %in% unique(m80vh21Cortex$structure.name)),], hum21pcwBCols[which(hum21pcwBCols$structure_name %in% unique(m80vh21Cortex$structure.name)),])

th15cenpj <- t(hum15pcwCenpj)
th15cenpj <- cbind(th15cenpj, hum15meta)
th15cenpj$age <- "15pcw"

th16cenpj <- t(hum16pcwCenpj)
th16cenpj <- cbind(th16cenpj, hum16meta)
th16cenpj$age <- "16pcw"

th21cenpj <- t(hum21pcwCenpj)
th21cenpj <- cbind(th21cenpj, hum21meta)
th21cenpj$age <- "21pcw"

thAllCenpj <- rbind(th15cenpj, th16cenpj, th21cenpj)

meltAllHCenpj <- melt(thAllCenpj, measure.vars = 1:3 )
meltAllHCenpj <- drop.levels(meltAllHCenpj)

meltAllHCenpj$variable <- gsub("15418", "Hs.probe1", meltAllHCenpj$variable)
meltAllHCenpj$variable <- gsub("15419", "Hs.probe2", meltAllHCenpj$variable)
meltAllHCenpj$variable <- gsub("15420", "Hs.probe3", meltAllHCenpj$variable)

# Reorder factors:
meltAllHCenpj$age <- factor(meltAllHCenpj$age, c("15pcw", "16pcw", "21pcw"))
meltAllHCenpj$probeage <- factor(paste(meltAllHCenpj$age, meltAllHCenpj$variable, sep="_"))

Hsprobe1 <- meltAllHCenpj[meltAllHCenpj$variable == "Hs.probe1",]
Hsprobe2 <- meltAllHCenpj[meltAllHCenpj$variable == "Hs.probe2",]
Hsprobe3 <- meltAllHCenpj[meltAllHCenpj$variable == "Hs.probe3",]

pdf(file="CENPJ_all_human_summary_plots.pdf", width=15)
    ggplot(meltAllHCenpj, aes(y=value, x=age, fill=variable)) + geom_boxplot(width=0.75, alpha=0.3) + theme_bw() + labs(y="qqnorm'ed intensity", x="Age", title="CENPJ by age and probe")
    ggplot(meltAllHCenpj, aes(y=value, x=age, fill=variable)) + geom_boxplot(width=0.75, alpha=0.3) + theme_bw() + labs(y="qqnorm'ed intensity", x="Age", title="CENPJ by probe, age and structure") + facet_grid(variable ~ structure_acronym) + theme(axis.text.x = element_text(angle=45))
    ggplot(meltAllHCenpj, aes(y=value, x=variable, fill=age)) + geom_boxplot(width=0.75, alpha=0.3) + theme_bw() + labs(y="qqnorm'ed intensity", x="", title="CENPJ by probe and age")
    ggplot(Hsprobe1, aes(y=value, x=age, fill=structure_name)) + geom_boxplot(width=0.75, alpha=0.3) + geom_point(aes(y=value, colour=structure_name), position=position_dodge(width=0.6)) + labs(y="qqnorm'ed intensity", x="Age", title="CENPJ Probe 1 by structure") + theme_bw()
    ggplot(Hsprobe2, aes(y=value, x=age, fill=structure_name)) + geom_boxplot(width=0.75, alpha=0.3) + geom_point(aes(y=value, colour=structure_name), position=position_dodge(width=0.6)) + labs(y="qqnorm'ed intensity", x="Age", title="CENPJ Probe 2 by structure") + theme_bw()
    ggplot(Hsprobe3, aes(y=value, x=age, fill=structure_name)) + geom_boxplot(width=0.75, alpha=0.3) + geom_point(aes(y=value, colour=structure_name), position=position_dodge(width=0.6)) + labs(y="qqnorm'ed intensity", x="Age", title="CENPJ Probe 3 by structure") + theme_bw()
    ggplot(meltAllHCenpj, aes(y=value, x=probeage, fill=structure_name)) + geom_boxplot(width=0.75, alpha=0.3) + theme_bw() + theme(axis.text.x = element_text(angle = 45, hjust = 1)) + labs(y="qqnorm'ed intensity", x="Age * Probe", title="CENPJ all probes by structure")
dev.off()

### In the supplementary data to Miller et al 2014b (the one about normalising arrays, not the brain transcriptome paper) they identify Hs.probe3 as the one that most closely mirrors RNA-seq results. As such, we will use this one for our figures. (This is done on the basis of the R column, which is the Pearson correlation between array and RNA-seq)

## First, rename acronyms and structures so they are matched across species:
Hsprobe3$structure_name2 <- gsub("SZ", "subventricular zone", Hsprobe3$structure_name)
Hsprobe3$structure_name2 <- gsub("VZ", "ventricular zone", Hsprobe3$structure_name2)
Hsprobe3$structure_name2 <- gsub("primary ", "", Hsprobe3$structure_name2)
Hsprobe3$structure_name2 <- gsub("inner |outer ", "", Hsprobe3$structure_name2)
Hsprobe3$structure_name2 <- gsub("in ", "of ", Hsprobe3$structure_name2)

# Create shape column to keep track of the origin of structures. 
Hsprobe3$shape <- as.factor(ifelse(grepl("inner", Hsprobe3$structure_name), "Inner", ifelse(grepl("outer", Hsprobe3$structure_name), "Outer", "Whole")))


pdf("human_qnorm_best_CENPJ_probe.pdf", width=15, height=4)
ggplot(Hsprobe3, aes(y=value, x=age, colour=structure_name)) + geom_point(size=2) + stat_smooth(aes(y=value,x=as.numeric(age))) +  facet_grid( . ~ structure_name) + theme_bw() + theme(legend.position="n") + labs(y="Z-transformed probe intensity", title="CENPJ best probe human (A_32_P219116), LOESS") + ylim(-2,2)
ggplot(Hsprobe3, aes(y=value, x=age, colour=structure_name)) + geom_point(size=2) + stat_smooth(aes(y=value,x=as.numeric(age)), method="lm") +  facet_grid( . ~ structure_name) + theme_bw() + theme(legend.position="n") + labs(y="Z-transformed probe intensity", title="CENPJ best probe human (A_32_P219116), LM") +ylim(-2,2)
ggplot(Hsprobe3, aes(y=value, x=age, colour=structure_name2)) + geom_point(aes(shape=shape), size=2) + stat_smooth(aes(y=value,x=as.numeric(age))) +  facet_grid( . ~ structure_name2) + theme_bw() + guides(colour=FALSE, shape=guide_legend(title=NULL)) + labs(y="Z-transformed probe intensity", title="CENPJ best probe human (A_32_P219116), LOESS") + ylim(-2,2)
ggplot(Hsprobe3, aes(y=value, x=age, colour=structure_name2)) + geom_point(aes(shape=shape), size=2) + stat_smooth(aes(y=value,x=as.numeric(age)), method="lm") +  facet_grid( . ~ structure_name2) + theme_bw() + guides(colour=FALSE, shape=guide_legend(title=NULL))  + labs(y="Z-transformed probe intensity", title="CENPJ best probe human (A_32_P219116), LM") +ylim(-2,2)
dev.off()





















##################################################
### 6. ADVENTURES IN AGGRESSIVE NORMALISATION. ###
##################################################

# Obsolete, qnorming incorporated into steps 4 and 5 to generate somewhat comparable metrics.

# Any sort of finessed normalisation will end in disaster, so better to just brute force it. First calculate ranks for every array:
mac70Filt <- mac70Filt[,!is.na(mac70Filt[1,])] # remove that NA column because it breaks things... but keep it in mind when matching names and structures - the missing column is called V254 and corresponds to row 253
mac70Rank <- data.frame(apply(mac70Filt, 2, function(x) qqnorm(x, plot.it=F)))
mac80Rank <- data.frame(apply(mac80Filt, 2, function(x) qqnorm(x, plot.it=F)))

hum16pcwRank <- data.frame(apply(hum16pcwFilt, 2, function(x) qqnorm(x, plot.it=F)))
hum21pcwRank <- data.frame(apply(hum21pcwFilt, 2, function(x) qqnorm(x, plot.it=F)))

# Then pull out CENPJ probes by species, keeping only the 'x' column (contains the qqnormed values)
mac70cenpj <- mac70Rank[which(macRows$gene_symbol %in% "CENPJ"), grepl("x", colnames(mac70Rank))]
mac80cenpj <- mac80Rank[which(macRows$gene_symbol %in% "CENPJ"), grepl("x", colnames(mac80Rank))]
hum16pcwCenpj <- hum16pcwRank[which(hum16pcwRows$gene_symbol %in% "CENPJ"), grepl("x", colnames(hum16pcwRank))]
hum21pcwCenpj <- hum21pcwRank[which(hum21pcwARows$gene_symbol %in% "CENPJ"), grepl("x", colnames(hum21pcwRank))] # Can use A or B, totally irrelevant

# Add some meta data to this, grabbing it from macCols and hum16pcwCols because it will respect the order we're interested in:
m70meta <- macCols[which(macCols$structure_name %in% macCortex & macCols$age == "E70"),]
m70meta <- m70meta[!(m70meta$structure_acronym %in% "rCGszi" & m70meta$donor_name %in% "DAM35650"),]
hum16meta <- hum16pcwCols[which(hum16pcwCols$structure_name %in% humanCortex),]

tm70cenpj <- t(mac70cenpj)
tm70cenpj <- cbind(tm70cenpj, m70meta)
meltm70Cenpj <- melt(tm70cenpj, measure.vars = 1:3 )
meltm70Cenpj$species <- "macaque"
meltm70Cenpj$variable <- gsub("10247", "Mm.probe1", meltm70Cenpj$variable)
meltm70Cenpj$variable <- gsub("10581", "Mm.probe2", meltm70Cenpj$variable)
meltm70Cenpj$variable <- gsub("17141", "Mm.probe3", meltm70Cenpj$variable)

th16cenpj <- t(hum16pcwCenpj)
th16cenpj <- cbind(th16cenpj, hum16meta)
melth16Cenpj <- melt(th16cenpj, measure.vars = 1:3 )
melth16Cenpj$species <- "human"
melth16Cenpj$variable <- gsub("15418", "Hs.probe1", melth16Cenpj$variable)
melth16Cenpj$variable <- gsub("15419", "Hs.probe2", melth16Cenpj$variable)
melth16Cenpj$variable <- gsub("15420", "Hs.probe3", melth16Cenpj$variable)
melth16Cenpj$age <- "16pcw"

meltm70h16 <- smartbind(meltm70Cenpj, melth16Cenpj)
    earlyPlot <- ggplot(meltm70h16, aes(y=value, x=species, fill=variable)) + geom_boxplot(width=0.75, alpha=0.3) + theme_bw() + labs(y="QQnorm intensity (rank)", x="", title="E70 vs 16PCW")+ guides(fill=F)

### And later on...
m80meta <- macCols[which(macCols$structure_name %in% macCortex & macCols$age == "E80"),]
hum21meta <- rbind(hum21pcwACols[which(hum21pcwACols$structure_name %in% unique(m80vh21Cortex$structure.name)),], hum21pcwBCols[which(hum21pcwBCols$structure_name %in% unique(m80vh21Cortex$structure.name)),])

tm80cenpj <- t(mac80cenpj)
tm80cenpj <- cbind(tm80cenpj, m80meta)
meltm80Cenpj <- melt(tm80cenpj, measure.vars = 1:3 )
meltm80Cenpj$species <- "macaque"
meltm80Cenpj$variable <- gsub("10247", "Mm.probe1", meltm80Cenpj$variable)
meltm80Cenpj$variable <- gsub("10581", "Mm.probe2", meltm80Cenpj$variable)
meltm80Cenpj$variable <- gsub("17141", "Mm.probe3", meltm80Cenpj$variable)

th21cenpj <- t(hum21pcwCenpj)
th21cenpj <- cbind(th21cenpj, hum21meta)
melth21Cenpj <- melt(th21cenpj, measure.vars = 1:3 )
melth21Cenpj$species <- "human"
melth21Cenpj$variable <- gsub("15418", "Hs.probe1", melth21Cenpj$variable)
melth21Cenpj$variable <- gsub("15419", "Hs.probe2", melth21Cenpj$variable)
melth21Cenpj$variable <- gsub("15420", "Hs.probe3", melth21Cenpj$variable)
melth21Cenpj$age <- "21pcw"

meltm80h21 <- smartbind(meltm80Cenpj, melth21Cenpj)
    latePlot <- ggplot(meltm80h21, aes(y=value, x=species, fill=variable)) + geom_boxplot(width=0.75, alpha=0.3) + theme_bw() + labs(y="QQnorm intensity (rank)", x="", title="E80 vs 21PCW") + guides(fill=F)

# And together...
pdf(file="qqnorm_compare_all.pdf", width=9)
    grid.arrange(earlyPlot, latePlot, ncol=2)

superMelt <- smartbind(meltm70h16, meltm80h21)
    ggplot(superMelt, aes(y=value, x=age, fill=variable)) + geom_boxplot(width=0.75, alpha=0.3) + theme_bw() + labs(y="QQnorm intensity (rank)", x="", title="CENPJ across time")

dev.off()

# If you look within a species you should go into limma or affy and normalise properly, but for now, a check of the drop of expression over time in macaques:











