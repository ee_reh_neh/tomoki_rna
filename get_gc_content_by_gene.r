#!/usr/bin/Rscript 

### For calculating GC content on a per gene basis rather than per exon basis. 
### Called by get_gc_content_by_gene.sh, see that script for additional documentation.
###
### IGR, 16.08.17

######################################
### 0. READ LINES & PROCESS FILES. ###
######################################

gcExon <- read.table(file("stdin"), sep="\t")
fileName <- commandArgs(trailingOnly=T)

library(plyr)
gcSums <- ddply(gcExon, "V2", function(x) colSums(x[,3:5]))
#print(head(gcSums))
#print (str(gcSums))

gcSums$gc <- (gcSums$V3+gcSums$V4) / gcSums$V5
write.table(gcSums, file=fileName, row.names=F, col.names=F, quote=F, sep="\t")
