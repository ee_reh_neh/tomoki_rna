### expression_eda.R
### For experimental analysis of RNA-seq data from human and crab-eating macaque neural progenitors. 
### Modelled closely on main_analysis_final.R from IGR's chimp-human iPSC paper
### IGR 16.08.14

### 0. PREPARE WORKSPACE. ###
### 1. READ IN COUNT MATRICES AND EXON LISTS. ### 
### 2. CALCULATE NORMALISED LIBRARY SIZES, GENERATE BASIC DESCRIPTIVE PLOTS. ###
### 3. TEST ORTHOLOGOUS EXON FILE VS FULL ENSEMBL84 MAPPING IN HUMANS ###
### 4. COMPUTE CPM, FILTER, LOESS NORMALISE, AND COMPUTE RPKM. ###
### 5. LOOK AT REPRODUCIBILITY BETWEEN DIFFERENT LEVELS OF REPLICATES. ###
### 6. PCA, TESTING POSSIBLE COVARIATES AND DRIVERS, AND OTHER BASIC VISUAL DESCRIPTIONS. ###
### 7. DIFFERENTIAL EXPRESSION TESTING. ###
### 8. LOTS OF PLOTS TO ASSESS GOODNESS OF FIT AND LOOK AT DE GENES. ###
### 9. ANNOTATE TOP GENES AND DO BASIC GO TESTING. ###


#############################
### 0. PREPARE WORKSPACE. ###
#############################

#extraVars <- commandArgs(trailingOnly=T)
extraVars <- c("ortho_TRUE", 0.92) 

### load libraries
library("gplots")
library(RColorBrewer)
library(limma)
library(edgeR)
library(statmod)
library(beeswarm)
library(biomaRt)
library(topGO)
library(SparseM)
library(org.Hs.eg.db)
library(variancePartition)

options(width=200)
setwd("~/tomoki/rpkm/")
pal <- c(brewer.pal(6, "BuPu"), brewer.pal(5, "YlOrRd"))

#################################################
### 1. READ IN COUNT MATRICES AND EXON LISTS. ### 
#################################################

hsOrthoCounts <- read.table(paste0("human_", extraVars[2], "pc_th_first_fc_2_", extraVars[1], "_clean.out"), header=T)
mfOrthoCounts <- read.table(paste0("macFas_", extraVars[2], "pc_th_first_fc_2_", extraVars[1], "_clean.out"), header=T)

mergedCounts <- merge(hsOrthoCounts[,c(1,7:13)], mfOrthoCounts[,c(1,7:11)], by.x="Geneid", by.y="Geneid", all=T)
names(mergedCounts) <- gsub("\\.fastq","", names(mergedCounts))
dim(mergedCounts)
# [1] 17057    13

# Remove unwanted individiuals - the failed individual with very very few reads overall: 
mergedCounts <- mergedCounts[,!grepl("EDI2.8_S5|H9.8_S3", colnames(mergedCounts))]

### OPTIONAL THINGS TO DO:
    # Filter all ribosomal protein genes because they are terrible:
    ensembl <- useMart("ENSEMBL_MART_ENSEMBL", dataset="hsapiens_gene_ensembl", host="mar2016.archive.ensembl.org") # Ensembl 84, for continuity.
    geneFamilies <- getBM(attributes=c("ensembl_gene_id", "family", "family_description"), mart=ensembl)
    badRibosomes <- geneFamilies[grepl("MRP|RPS|RPL|RP L|RP S", geneFamilies$family_description),1] # Filtering genes associated with nasty ribosomal families - I wonder how effective this will be downstream!

    mergedCounts <- mergedCounts[!mergedCounts$Geneid %in% badRibosomes,]
    dim(mergedCounts)
    # [1] 16991    11

    # For RPKM calculation later
    mergedCounts$hsLength <- hsOrthoCounts[!hsOrthoCounts$Geneid %in% badRibosomes,6]
    mergedCounts$mfLength <- mfOrthoCounts[!mfOrthoCounts$Geneid %in% badRibosomes,6]

    # drop non-autosomal genes
    # ychr.drop <- c("ENSG00000183878", "ENSG00000198692", "ENSG00000234652", "ENSG00000243576", "ENSG00000252472")
    # gene.lengths <- gene.lengths[!gene.lengths$GeneEnsemblID %in% ychr.drop,]

#prepare meta information from sample names:
countsNames <- colnames(mergedCounts[,2:11])
speciesCol <- c(rep("darkorchid4", 5), rep("orange", 5))
samplesMeta <- data.frame(countsNames, speciesCol)
names(samplesMeta) <- c("line", "col")
samplesMeta$cex <- 1.5

    #alternative colours etc:
    samplesMeta$species <- ifelse(grepl("^M", samplesMeta$line), "Mf", "Hs")
    samplesMeta$ind <- ifelse(grepl("^MF12", samplesMeta$line), "Mf12", ifelse(grepl("^MFTW", samplesMeta$line), "MfTW1", ifelse(grepl("^E", samplesMeta$line), "EDI2", "H9")))
    samplesMeta$sex <- ifelse(grepl("^MF12", samplesMeta$line), "M", "F")
    samplesMeta$indPch <- 14 + as.numeric(as.factor(samplesMeta$ind))

    indPal <- brewer.pal(4, "Dark2")
    samplesMeta$indPal <- indPal[as.numeric(as.factor(samplesMeta$ind))]

	# and this is for EDAseq:
	row.names(samplesMeta) <- samplesMeta$line

# save(samplesMeta, file="samples_meta_expression_final.Rda")

################################################################################
### 2. CALCULATE NORMALISED LIBRARY SIZES, GENERATE BASIC DESCRIPTIVE PLOTS. ###
################################################################################

# reads into edgeR, calculate TMM and then CPM, writing out the intermediate steps:
mergedCountsDge <- DGEList(counts=as.matrix(mergedCounts[,2:11]), genes=mergedCounts[,1])
mergedCountsDge <- calcNormFactors(mergedCountsDge)
#save(mergedCountsDge, file="ipsc_genes_dge_ipsc_final.Rda")
#write.table(mergedCountsDge$samples, file="TMM.normFactors_ipsc_genes_ipsc_final.txt", sep="\t", quote=F, row.names=T)

## some barplots about mapping stats
## "Raw" library sizes:
pdf(file="eda_plots/mapped_reads_raw_ipsc_clean.drop_inds.pdf")
mp <- barplot(sort(colSums(mergedCountsDge$counts)), ylab="Number of reads mapped to orthologous exons", xlab="", col="darkgrey", xaxt="n")
text(mp, -200000, srt = 45, adj = 1, labels = names(sort(colSums(mergedCountsDge$counts))), xpd = TRUE, cex=0.8)
dev.off()

# normalised library sizes
zpdf(file="eda_plots/mapped_reads_normalised_ipsc_clean.drop_inds.pdf")
mp <- barplot(sort(mergedCountsDge$samples$lib.size * mergedCountsDge$samples$norm.factor), ylab="Normalized library sizes", xlab="", xaxt="n", col="darkgrey")
text(mp, -200000, srt = 45, adj = 1, labels = row.names(mergedCountsDge$samples[order(mergedCountsDge$samples$lib.size * mergedCountsDge$samples$norm.factor), ]), xpd = TRUE, cex=0.8)
dev.off()

## number of genes expressed
pdf(file="eda_plots/number_of_genes_expressed_ipsc_clean.drop_inds.pdf")
some.counts <- apply(mergedCountsDge$counts, 2, function(x) { sum(x > 0) })
mp <- barplot(sort(some.counts), ylab="Number of genes with at least 1 read", xlab="", xaxt="n", col="darkgrey")
text(mp, -500, srt = 45, adj = 1, labels = names(sort(some.counts)), xpd = TRUE, cex=0.8)
dev.off()

## Perform rarefaction curves for number of expressed genes vs. proportion of pool mRNA
## As in Ramskold D, Wang ET, Burge CB, Sandberg R. 2009. An abundance of ubiquitously expressed genes revealed by tissue transcriptome sequence data. PLoS Comput Biol 5:e1000598.
## This gives an idea of the complexity of transcriptome in different tissues

## using ortho Exons genes aggregates
pdf(file="eda_plots/rarefaction_curves_ipsc_clean.drop_inds.pdf")
plot(1:length(mergedCountsDge$counts[,1]), cumsum(sort(mergedCountsDge$counts[,1], decreasing=T)/sum(mergedCountsDge$counts[,1])), log="x", type="n", xlab="Number of genes", ylab="Fraction of reads pool", ylim=c(0,1)) ## initialize the plot area
for (sample in colnames(mergedCountsDge)){
  lines(1:length(mergedCountsDge$counts[,sample]), cumsum(sort(mergedCountsDge$counts[,sample], decreasing=T)/sum(mergedCountsDge$counts[,sample])), col=pal[which(grepl(sample, colnames(mergedCountsDge)))], lwd=2)
#  lines(1:length(mergedCountsDge$counts[,sample]), cumsum(sort(mergedCountsDge$counts[,sample], decreasing=T)/sum(mergedCountsDge$counts[,sample])), col=as.character(samplesMeta[samplesMeta$line %in% sample,]$col), lwd=2)
}
legend(x="topleft", bty="n", col=c(pal), legend=samplesMeta$line, lty=1, lwd=2)
dev.off()


##########################################################################
### 3. TEST ORTHOLOGOUS EXON FILE VS FULL ENSEMBL84 MAPPING IN HUMANS. ###
##########################################################################

# # This is quick and dirty, and does not bother to control for possible covariates - but it shouldn't really have to. All I want is the correlation by sample of CPM before and after filtering for orthologous exons. 

# # Read in whole-exon data:
# hg38Counts <- read.table("ensembl84_testing/human_ensembl84_th_firststrand_fc_secondstrand_clean.drop_inds.out", header=T)

# # make them into DGE lists, excluding the low depth individual
# hg38CountsDge <- DGEList(counts=hg38Counts[,c(7:11,13)], genes=hg38Counts[,1])
# hg38CountsDge <- calcNormFactors(hg38CountsDge)
# hsOrthoDge <- DGEList(counts=hsOrthoCounts[,c(7:11,13)], genes=hsOrthoCounts[,1])
# hsOrthoDge <- calcNormFactors(hsOrthoDge)

# # Count number of genes with at least one read mapped to them before and after ortho exon filter:
# cbind(apply(hg38CountsDge$counts, 2, function(x) { sum(x > 0) }), apply(hsOrthoDge$counts, 2, function(x) { sum(x > 0) }))

# # Calculate CPM and RPKM
# hg38CountsCPM <- cpm(hg38CountsDge, normalized.lib.sizes=TRUE, log=TRUE, prior.count=0.25)
# hsOrthoCPM <- cpm (hsOrthoDge, normalized.lib.sizes=TRUE, log=TRUE, prior.count=0.25)

# hg38CountsRPKM <- as.data.frame(hg38CountsCPM[,1:6] - log2((hg38Counts$Length)/1000))
# hsOrthoRPKM <- as.data.frame(hsOrthoCPM[,1:6] - log2((hsOrthoCounts$Length)/1000))

# hg38CountsRPKM$genes <- hg38Counts[,1]
# hsOrthoRPKM$genes <- hsOrthoCounts[,1]

# humanMerge <- merge(hg38CountsRPKM, hsOrthoRPKM, by.x="genes", by.y="genes", all=F)
# names(humanMerge) <- gsub("first_strand", "", names(humanMerge))
# names(humanMerge)[2:7] <- gsub("\\.fastq", "full", names(humanMerge)[2:7])
# names(humanMerge)[8:13] <- gsub("\\.fastq", "ortho", names(humanMerge)[8:13])

# pdf("eda_plots/human_full_ortho_test_pearson.pdf")
# heatmap.2(as.matrix(cor(humanMerge[,2:13], method="pearson")), trace="none", col=rev(colorRampPalette(brewer.pal(9, "Blues"))(30)), ColSideColors=rep(brewer.pal(6, "Dark2"),2), RowSideColors=rep(brewer.pal(6, "Dark2"),2), main="Pearson correlation, human samples", margins=c(6,6))
# dev.off()

# pdf("eda_plots/human_full_ortho_test_spearman.pdf")
# heatmap.2(as.matrix(cor(humanMerge[,2:13], method="spearman")), trace="none", col=rev(colorRampPalette(brewer.pal(9, "Blues"))(30)), ColSideColors=rep(brewer.pal(6, "Dark2"),2), RowSideColors=rep(brewer.pal(6, "Dark2"),2), main="Spearman correlation, human samples", margins=c(6,6))
# dev.off()

# # And now see what happens if you filter out lowly expressed genes:
# # Calculate CPM and RPKM
# hg38CountsFiltDge <- hg38CountsDge[(rowSums(hg38CountsCPM[,1:6] > 1) >= 3), ] 
# hsOrthoFiltDge <- hsOrthoDge[(rowSums(hsOrthoCPM[,1:6] > 1) >= 3), ] 

# hg38CountsFiltCPM <- cpm(hg38CountsFiltDge, normalized.lib.sizes=TRUE, log=TRUE, prior.count=0.25)
# hsOrthoFiltCPM <- cpm (hsOrthoFiltDge, normalized.lib.sizes=TRUE, log=TRUE, prior.count=0.25)

# hg38CountsFiltRPKM <- as.data.frame(hg38CountsFiltCPM[,1:6] - log2((hg38Counts[(rowSums(hg38CountsCPM[,1:6] > 1) >= 3),6])/1000))
# hsOrthoFiltRPKM <- as.data.frame(hsOrthoFiltCPM[,1:6] - log2((hsOrthoCounts[(rowSums(hsOrthoCPM[,1:6] > 1) >= 3),6])/1000))

# hg38CountsFiltRPKM$genes <- hg38Counts[(rowSums(hg38CountsCPM[,1:6] > 1) >= 3),1]
# hsOrthoFiltRPKM$genes <- hsOrthoCounts[(rowSums(hsOrthoCPM[,1:6] > 1) >= 3),1]

# humanFiltMerge <- merge(hg38CountsFiltRPKM, hsOrthoFiltRPKM, by.x="genes", by.y="genes", all=F)
# names(humanFiltMerge) <- gsub("first_strand", "", names(humanFiltMerge))
# names(humanFiltMerge)[2:7] <- gsub("\\.fastq", "full", names(humanFiltMerge)[2:7])
# names(humanFiltMerge)[8:13] <- gsub("\\.fastq", "ortho", names(humanFiltMerge)[8:13])

# pdf("eda_plots/human_full_ortho_test_filtered_pearson.pdf")
# heatmap.2(as.matrix(cor(humanFiltMerge[,2:13], method="pearson")), trace="none", col=rev(colorRampPalette(brewer.pal(9, "Blues"))(30)), ColSideColors=rep(brewer.pal(6, "Dark2"),2), RowSideColors=rep(brewer.pal(6, "Dark2"),2), main="Pearson correlation, human samples", margins=c(6,6))
# dev.off()

# pdf("eda_plots/human_full_ortho_test_filtered_spearman.pdf")
# heatmap.2(as.matrix(cor(humanFiltMerge[,2:13], method="spearman")), trace="none", col=rev(colorRampPalette(brewer.pal(9, "Blues"))(30)), ColSideColors=rep(brewer.pal(6, "Dark2"),2), RowSideColors=rep(brewer.pal(6, "Dark2"),2), main="Spearman correlation, human samples", margins=c(6,6))
# dev.off()

# # And now clean.drop_inds up after yourself...
# rm(hg38CountsDge, hsOrthoDge, hg38CountsCPM, hsOrthoCPM, hg38CountsRPKM, hsOrthoRPKM, humanMerge, hg38CountsFiltDge, hsOrthoFiltDge, hg38CountsFiltCPM, hsOrthoFiltCPM, hg38CountsFiltRPKM, hsOrthoFiltRPKM, humanFiltMerge)

# # NB can probably delete the hg38 counts file too...


##################################################################
### 4. COMPUTE CPM, FILTER, LOESS NORMALISE, AND COMPUTE RPKM. ###
##################################################################

### Calculate log2 CPM
cpmNorm <- cpm(mergedCountsDge, normalized.lib.sizes=TRUE, log=TRUE, prior.count=0.25) ### I may come back and try this with the Irizarry prior of 0.5, because it makes more intuitive sense to me... but this one is simply -2, so really, no difference
pdf(file="eda_plots/cpm.density_ipsc_clean.drop_inds.pdf")
plotDensities(cpmNorm, group=samplesMeta$species) 
abline(v=1)
dev.off()

# Filter on observing cpm greater or equal to 1 or more in at least half of the individuals in one species, not keeping library sizes.
mergedCountsDgeFilt <- mergedCountsDge[rowSums(cpmNorm[,1:5] >= 1) >= 3 | rowSums(cpmNorm[,6:10] >= 1) >= 3 , , keep.lib.sizes=F] 
dim(mergedCountsDgeFilt)
#[1] 11343    11

pdf(file="eda_plots/cpmFilt.density_ipsc_clean.drop_inds.pdf")
plotDensities(mergedCountsDgeFilt, group=samplesMeta$species)
dev.off()

# Recalculate TMM and lib sizes
mergedCountsDgeFilt <- calcNormFactors(mergedCountsDgeFilt) ## recalculate norm factors
# save(mergedCountsDgeFilt, file="mergedCountsDge.TMMFilt_clean.drop_inds.RDa")

# Loess normalise the data
## For starters, Voom requires a design matrix as input
## To make contrasts easier to formulate, we rename factors species and tissue in a single factor
design <- model.matrix(~ 0 + samplesMeta$species)
colnames(design) <- c("human", "macFas")

## Voom on filtered nonGC normalized data, with cyclic loess normalization, and blocked by individual replicates - this requires two passes, one without the random individual effect and a second one that takes it into account - see the limma manual and this post and reply by Gordon Smyth!
## https://support.bioconductor.org/p/59700/

## note that cyclic loess is designed for between-array normalisation rather than RNA-seq, but it should still be usable. Ideally we would not need it, however, but I don't like the directionality of the results without normalisation, not to mention there's a clear outlier sample in there. 
cpmNormLoess <- voom(mergedCountsDgeFilt, design, normalize.method="cyclicloess", plot=F) 
cpmCorfit <- duplicateCorrelation(cpmNormLoess, design, block=samplesMeta$ind)
    #Gives NO warnings, but out of 13k, that's hardly a concern. 
cpmCorfit$consensus
# [1] 0.3697038
pdf(file="eda_plots/voom.loess.cpm.indrandom.output_clean.drop_inds.pdf")
cpmNormLoess <- voom(mergedCountsDgeFilt, design, normalize.method="cyclicloess", plot=T, correlation=cpmCorfit$consensus, block=samplesMeta$ind) 
dev.off()

# Second round of duplicate correlations, as recommended by Gordon Smyth. 
cpmCorfit <- duplicateCorrelation(cpmNormLoess, design, block=samplesMeta$ind)
    #Gives NO warnings the second time around. 
cpmCorfit$consensus
# [1] 0.3695179

pdf(file="eda_plots/voom.loess.cpm.indrandom.density_clean.drop_inds.pdf")
plotDensities(cpmNormLoess, group=samplesMeta$species) 
dev.off()

## Turn this into RPKM
# Values are already log2 CPM, so just need to substract
rpkmNormLoess <- cpmNormLoess
rpkmNormLoess$E[,1:5] <- cpmNormLoess$E[,1:5] - log2((mergedCounts[rowSums(cpmNorm[,1:5] > 1) >= 3 | rowSums(cpmNorm[,6:10] > 1) >= 3 , 12])/1000)
rpkmNormLoess$E[,6:10] <- cpmNormLoess$E[,6:10] - log2((mergedCounts[rowSums(cpmNorm[,1:5] > 1) >= 3 | rowSums(cpmNorm[,6:10] > 1) >= 3 , 13])/1000)

pdf(file="eda_plots/voom.loess.rpkm.indrandom.density_clean.drop_inds.pdf")
plotDensities(rpkmNormLoess, group=samplesMeta$species) #again, a beautiful distribution, as expected. 
dev.off()

# It's a very nice distribution, not quite identical between species, but still better than other data I have seen. For now we'll keep it.

##########################################################################
### 5. LOOK AT REPRODUCIBILITY BETWEEN DIFFERENT LEVELS OF REPLICATES. ###
##########################################################################

plot.reproducibility <- function(data.to.test, metadata, method){
    cor.mat <- cor(data.to.test, method=method, use="pairwise.complete.obs")

    ind.rep <- vector()
    ind.rep.col <- vector()
    species.rep <- vector()
    between.species <- vector()

    for (i in 1:ncol(data.to.test)){
        for (j in 1:ncol(data.to.test)){
            if (j > i){
                if (metadata$ind[i] == metadata$ind[j]) {
                       ind.rep <- c(ind.rep, cor.mat[i,j])
                       ind.rep.col <- c(ind.rep.col, metadata$indPal[i])
                } else if (metadata$species[i] == metadata$species[j]) {species.rep <- c(species.rep, cor.mat[i,j])
                } else {between.species <- c(between.species, cor.mat[i,j])}
            }
        }
    }

    for.plot <- list(ind.rep, species.rep, between.species)
    boxplot(for.plot, ylab=paste0(method, " correlation"), names=c("Within\nindividuals", "Within\nspecies", "Between\nspecies"))
    beeswarm(for.plot[1], vertical = TRUE, method = "swarm", add = TRUE, pch = 20, cex=2, pwcol=ind.rep.col)
    stripchart(for.plot[2:3], vertical = TRUE, method = "jitter", add = TRUE, pch = 20, cex=2, at=2:3, col="grey50")
    legend(x="topright", legend=unique(metadata$ind), col=unique(metadata$indPal), bty="n", pch=20, cex=1.3)
}

pdf(file="eda_plots/reproducibility_by_levels_clean.drop_inds.pdf")
plot.reproducibility(rpkmNormLoess$E, samplesMeta, "spearman")
plot.reproducibility(rpkmNormLoess$E, samplesMeta, "pearson")
dev.off()

#############################################################################################
### 6. PCA, TESTING POSSIBLE COVARIATES AND DRIVERS, AND OTHER BASIC VISUAL DESCRIPTIONS. ###
#############################################################################################

# Here is the PCA plotting function:
plot.pca <- function(dataToPca, speciesCol, namesPch, sampleNames){
    # check for invariant rows:
    dataToPca.clean.drop_inds <- dataToPca[!apply(dataToPca, 1, var) == 0,]
    pca <- prcomp(t(dataToPca.clean.drop_inds), scale=T, center=T)
    pca.var <- pca$sdev^2/sum(pca$sdev^2)
    plot(pca$x[,1], pca$x[,2], col=speciesCol, pch=namesPch, cex=2, xlab=paste("PC1 (", round(pca.var[1]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC2 (", round(pca.var[2]*100, digits=2), "% of variance)", sep=""))
    legend(col=speciesCol, legend=sampleNames, pch=namesPch, x="topright", cex=0.6)
    plot(pca$x[,2], pca$x[,3], col=speciesCol, pch=namesPch, cex=2, xlab=paste("PC2 (", round(pca.var[2]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC3 (", round(pca.var[3]*100, digits=2), "% of variance)", sep=""))
    legend(col=speciesCol, legend=sampleNames, pch=namesPch, x="topright", cex=0.6)
    plot(pca$x[,3], pca$x[,4], col=speciesCol, pch=namesPch, cex=2, xlab=paste("PC3 (", round(pca.var[3]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC4 (", round(pca.var[4]*100, digits=2), "% of variance)", sep=""))
    legend(col=speciesCol, legend=sampleNames, pch=namesPch, x="topright", cex=0.6)

    # all.pcs <- pc.assoc(pca)
    # print ("Here are the relationships between PCs and some possible covariates")
    # print (all.pcs)
    # return (all.pcs)
}

# And here is the PCA association function
# pc.assoc <- function(pca.data){
#     all.pcs <- data.frame()
#     for (i in 1:ncol(pca.data$x)){
#         all.assoc <- vector()
#         for (j in 1:ncol(all.covars.df)){
#             test.assoc <- anova(lm(pca.data$x[,i] ~ all.covars.df[,j]))[1,5]
#             all.assoc <- c(all.assoc, test.assoc)
#         }
#         single.pc <- c(i, all.assoc)
#         all.pcs <- rbind(all.pcs, single.pc)
#     }
#     names(all.pcs) <- c("PC", colnames(all.covars.df))
#     return(all.pcs)
# }

# Actually plotting the PCA
pdf(file="eda_plots/voom.loess.cpm.indrandom.pca_clean.drop_inds.pdf")
plot.pca(rpkmNormLoess$E, pal, samplesMeta$indPch, samplesMeta$line)
dev.off()

# RPKM correlation matrices
pdf(file="eda_plots/voom.loess.cpm.indrandom.clustering_clean.drop_inds.pdf")
par(cex.main=0.8)
heatmap.2(cor(rpkmNormLoess$E, method="pearson", use="pairwise.complete.obs"), trace="none", main="Pearson correlation, RPKM", margins=c(8,8), srtCol=45, srtRow=45)
heatmap.2(cor(rpkmNormLoess$E, method="spearman", use="pairwise.complete.obs"), trace="none", main="Spearman correlation, RPKM", margins=c(8,8), srtCol=45, srtRow=45)
dev.off()


###########################################
### 7. DIFFERENTIAL EXPRESSION TESTING. ###
###########################################

## Loess norm with individual blocking/random effect
rpkmNormLoessFit <- lmFit(rpkmNormLoess, design, block=samplesMeta$ind, correlation=cpmCorfit$consensus)

contrasts <- makeContrasts(
                HumanvsMacFas = (human - macFas),
                levels=design)

rpkmNormLoessFit2 <- contrasts.fit(rpkmNormLoessFit, contrasts)
rpkmNormLoessFit2 <- eBayes(rpkmNormLoessFit2)

topGenesLoess <- topTable(rpkmNormLoessFit2, adjust="BH", number=Inf, sort.by="p")
table(decideTests(rpkmNormLoessFit2, adjust.method="BH", method="separate", p.value=0.01))
  #  -1     0     1 
  # 176 10826   243 

table(decideTests(rpkmNormLoessFit2, adjust.method="BH", method="separate", p.value=0.05))
 #  -1    0    1 
 # 690 9785  770 

table(decideTests(rpkmNormLoessFit2, adjust.method="BH", method="separate", p.value=0.10))
#    -1    0    1 
# 1151 8883 1211 

#write.table(topGenesLoess, file="voom.loess.cpm.indrandom.de_testing_results.out", quote=F, row.names=F, col.names=T, sep="\t", eol="\n")

########################################################################
### 8. LOTS OF PLOTS TO ASSESS GOODNESS OF FIT AND LOOK AT DE GENES. ###
########################################################################

#Define the new plotting functions:
# The one that plots individual gene values:
check_results <- function(count_table, top, n) {
  cols <- colorRampPalette(pal)(n)
  rownames(count_table$E) <- count_table$genes$genes
  plot(1,1, xlim=c(1, length(count_table[1,])), ylim=c(min(count_table[rownames(count_table) %in% top$genes[1:n],]$E), max(count_table[rownames(count_table) %in% top$genes[1:n],]$E)), type="n", xaxt="n", ylab="Normalized expression", xlab="")
  text(1:length(count_table[1,]), min(count_table[rownames(count_table) %in% top$genes[1:n],]$E)-0.07*(max(count_table[rownames(count_table) %in% top$genes[1:n],]$E)-min(count_table[rownames(count_table) %in% top$genes[1:n],]$E)), srt = 45, adj = 1,labels = colnames(count_table), xpd = TRUE)
  axis(side=1, at=c(1:length(count_table[1,])), labels=rep("", dim(count_table$targets)[1]))
  abline(v=1:length(count_table[1,]), lty=3, col="lightgray")
 
  if (n > length(top[,1])){
    n <- length(top[,1])
  }
  for (i in 1:n){
    lines(1:length(count_table[1,]), count_table[rownames(count_table) %in% top$genes[i],]$E, col=cols[i])
   }
}

# The volcano plot:
volcano.plot <- function(gene.list, significance, outfile){
    pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_volcano_", outfile, ".pdf"))
    plot(gene.list$logFC, -log10(gene.list$adj.P.Val), xlab="log2 fold change", ylab="-log10 corrected P value", main=outfile, cex=0.75, pch=16, cex.lab=1.5, cex.axis=1.3, col=rgb(0,0,0,.75))
    abline(v = 0, lty=2, col=adjustcolor("red", alpha=0.8), lwd=2.5)
    abline(h = -log10(significance), lty=2, col=adjustcolor("red", alpha=0.8), lwd=2.5)
    legend(x = "topleft", paste(dim(gene.list[gene.list$adj.P.Val < significance & gene.list$logFC < 0,])[1], " genes\n(", dim(gene.list[gene.list$adj.P.Val < significance & gene.list$logFC < -1,])[1], " FC < -1)", sep=""), bty="n", cex=1.3)
    legend(x = "topright", paste(dim(gene.list[gene.list$adj.P.Val < significance & gene.list$logFC > 0,])[1], " genes\n(", dim(gene.list[gene.list$adj.P.Val < significance & gene.list$logFC > 1,])[1], " FC > 1)", sep=""), bty="n", cex=1.3)
    legend(x = "bottomleft", paste(dim(gene.list[gene.list$adj.P.Val > significance & gene.list$logFC < 0,])[1], " genes\n(", dim(gene.list[gene.list$adj.P.Val > significance & gene.list$logFC < -1,])[1], " FC < -1)", sep=""), bty="n", cex=1.3)
    legend(x = "bottomright", paste(dim(gene.list[gene.list$adj.P.Val > significance & gene.list$logFC > 0,])[1], " genes\n(", dim(gene.list[gene.list$adj.P.Val > significance & gene.list$logFC > 1,])[1], " FC > 1)", sep=""), bty="n", cex=1.3)
    dev.off()   
}

# Define the number of genes to plot:
gene.plots <- c(10,50,100,200,500,1000)
colors <- rev(colorRampPalette(brewer.pal(10,"RdBu"))(100)) ## We want red to be higher expression and blue lower

# volcano.plot(topGenes, 0.01, "all_genes_fdr_0.01")
# volcano.plot(topGenes, 0.05, "all_genes_fdr_0.05")
# volcano.plot(topGenes, 0.10, "all_genes_fdr_0.10")

# The actual plotting function, which makes all kinds of plots:

makeAllPlots <- function(deObject, fitObject, exprsObject, topTable, normName){
    # Model goodness plots:
    pdf(paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_voom.", normName, ".indblock.maplot.pdf"))
    par(mfrow=c(1,2))
    limma::plotMA(deObject, coef=1, xlab="average coefficient", ylab="estimated coefficient", main="human") ## for a bunch of exons with medium intensity (-2<cpm<2), in chimp heart they are either highly expressed or not expressed. Highly expressed genes (on average) seem to be less variable in individual samples
    abline(a=0, b=1,col=pal[1])
    limma::plotMA(deObject, coef=2, xlab="average coefficient", ylab="estimated coefficient", main="macFas")
    abline(a=0, b=1,col=pal[1])
    dev.off()

    # MA plot:
    pdf(paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_voom.", normName, ".indblock.mdplot.pdf"))
    plotMD(fitObject, coef=1, xlab="average expression", ylab="log2 FC")
    abline(h=0, col="red", lty=1, lwd=1)
    dev.off()

    ## Sigma vs A plot. After a linear model is fitted, this checks constancy of the variance with respect to intensity level.
    pdf(paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_voom.", normName, ".indblock.sigmaaplot.pdf"))
    plotSA(deObject, main="species")
    dev.off()

    ## Boxplot of the residuals (difference between the true value and fitted value)
    pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_voom.", normName, ".indblock.residuals.pdf"))
    hist(log2(deObject$sigma), breaks=100)
    boxplot(log2(deObject$sigma), names="species")
    dev.off()

    # DE plots
    for (i in 1:6){
        pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_voom.", normName, ".indblock.check_results_top_", gene.plots[i], ".pdf"))
        check_results(exprsObject, topTable, gene.plots[i])
        dev.off()

        pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_voom.", normName, ".indblock.heatmap_top_", gene.plots[i], ".pdf"))
        heatmap.2(exprsObject$E[rownames(topTable[1:gene.plots[i],]),], col = colors, margins = c(12, 12), trace='none', denscol="white", labCol=samplesMeta$species, labRow=NA, ColSideColors=pal[as.integer(as.factor(samplesMeta$species))], cexCol = 1.5)
        dev.off()
    }

    volcano.plot(topTable, 0.01, paste0(normName, "_all_genes_fdr_0.01"))
    volcano.plot(topTable, 0.05, paste0(normName, "_all_genes_fdr_0.05"))
    volcano.plot(topTable, 0.10, paste0(normName, "_all_genes_fdr_0.10"))
}

makeAllPlots(rpkmNormLoessFit, rpkmNormLoessFit2, rpkmNormLoess, topGenesLoess, "loess_clean.drop_inds")


######################################################
### 9. ANNOTATE TOP GENES AND DO BASIC GO TESTING. ###
######################################################

### define the testing function
de.testing <- function(gene.in, universe, ontology, suffix, algorithm){
    #pull the genes that are significant
    de.all <- ifelse(universe %in% gene.in, 1, 0)
    names(de.all) <- universe
    de.all <- as.factor (de.all)

    #test for DE: all
    for (i in 1){
        test.degenes <- new("topGOdata", ontology=ontology, allGenes=de.all, nodeSize = 5, annot = annFUN.org, mapping = "org.Hs.eg.db", ID = "ensembl")
        test.degenes.fisher <- runTest(test.degenes, algorithm = algorithm, statistic = "fisher")
        test.degenes.out <- GenTable(test.degenes, classicFisher = test.degenes.fisher, orderBy = "classicFisher", ranksOf = "classicFisher", topNodes = length(score(test.degenes.fisher)))
    
        #fix p-values
        test.degenes.out$BH <- p.adjust(test.degenes.out$classicFisher, method="BH")
         test.degenes.out$fisher.fix <- as.numeric(test.degenes.out$classicFisher)
         test.degenes.out[is.na(test.degenes.out$fisher.fix),8] <- 10^-30
#         test.degenes.out$qvalue <- qvalue(test.degenes.out$fisher.fix)$qvalue
       
        #and collecting summary stats
#        test.stats <- c("all", suffix, ontology, algorithm, length(gene.in), test.degenes.fisher@geneData[1], test.degenes.fisher@geneData[2], dim(test.degenes.out)[1], dim(test.degenes.out[test.degenes.out$classicFisher < 0.05,])[1],dim(test.degenes.out[test.degenes.out$BH < 0.05 | is.na(test.degenes.out$BH),])[1], dim(test.degenes.out[test.degenes.out$qvalue < 0.05,])[1])
        test.stats <- c("all", suffix, ontology, algorithm, length(gene.in), test.degenes.fisher@geneData[1], test.degenes.fisher@geneData[2], dim(test.degenes.out)[1], dim(test.degenes.out[test.degenes.out$classicFisher < 0.05,])[1],dim(test.degenes.out[test.degenes.out$BH < 0.05 | is.na(test.degenes.out$BH),])[1])

        
        #write result tables for future analysis    
        write.table(test.degenes.out, file=paste0("topGO_out/", ontology, "_", test.type, "_" ,suffix, "_", algorithm, ".out"), quote=F, row.names=F, sep="\t")
        
        #collating summary stats
        summary.stats <- as.data.frame(rbind(test.stats))
#        names(summary.stats) <- c("test", "tp", "ontology", "algorithm", "genes", "annot genes", "sig genes", "annot terms", "sig terms", "sig terms BH", "sig terms qvalue")
        names(summary.stats) <- c("test", "tp", "ontology", "algorithm", "genes", "annot genes", "sig genes", "annot terms", "sig terms", "sig terms BH")
        overall.stats <- rbind(overall.stats, summary.stats)
        
        #making plots!
        #printGraph(test.degenes, test.degenes.fisher, firstSigNodes=10, useInfo="all", fn.prefix=paste("decay_GO_", ontology, "_", test.type, "_" ,suffix, sep=""), pdfSW=T)
    }
    return(overall.stats)
}

#Prep the function:
overall.stats <- data.frame()
test.type <- "all"

overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05,]$genes, topGenesLoess$genes, "BP", "all_genes_fdr_0.05", "classic")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05,]$genes, topGenesLoess$genes, "MF", "all_genes_fdr_0.05", "classic")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01,]$genes, topGenesLoess$genes, "BP", "all_genes_fdr_0.01", "classic")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01,]$genes, topGenesLoess$genes, "MF", "all_genes_fdr_0.01", "classic")

overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "BP", "all_genes_fdr_0.05_logfc", "classic")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "MF", "all_genes_fdr_0.05_logfc", "classic")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "BP", "all_genes_fdr_0.01_logfc", "classic")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "MF", "all_genes_fdr_0.01_logfc", "classic")

overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05,]$genes, topGenesLoess$genes, "BP", "all_genes_fdr_0.05", "elim")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05,]$genes, topGenesLoess$genes, "MF", "all_genes_fdr_0.05", "elim")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01,]$genes, topGenesLoess$genes, "BP", "all_genes_fdr_0.01", "elim")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01,]$genes, topGenesLoess$genes, "MF", "all_genes_fdr_0.01", "elim")

overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "BP", "all_genes_fdr_0.05_logfc", "elim")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "MF", "all_genes_fdr_0.05_logfc", "elim")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "BP", "all_genes_fdr_0.01_logfc", "elim")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "MF", "all_genes_fdr_0.01_logfc", "elim")


# And a comparison with Steven Reilly's data:
reilly <- read.table("../reilley_et_al_2015_enriched_genes.txt", header=F)
length(which(topGenesLoess$genes %in% reilly$V1))
length(which(topGenesLoess[topGenesLoess$adj.P.Val <= 0.1,]$genes %in% reilly$V1))
length(which(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05,]$genes %in% reilly$V1))
length(which(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01,]$genes %in% reilly$V1))

### This is not the best way of testing, since we have no idea of how many genes there were in the Reilly data to begin with, but:
loessSig01 <- topGenesLoess[topGenesLoess$adj.P.Val <= 0.01,]
loessSig05 <- topGenesLoess[topGenesLoess$adj.P.Val <= 0.05,] 
loessSig10 <- topGenesLoess[topGenesLoess$adj.P.Val <= 0.10,] 

permutation.stats <- data.frame()

for (i in 1:100000) {
  #sample DE genes from all the data sets, array sized: 
  genesDE01 <- sample(1:dim(topGenesLoess)[1], dim(loessSig01)[1], replace=F)
  genesDE05 <- sample(1:dim(topGenesLoess)[1], dim(loessSig05)[1], replace=F)
  genesDE10 <- sample(1:dim(topGenesLoess)[1], dim(loessSig10)[1], replace=F)
  reillySig <- sample(1:dim(topGenesLoess)[1], dim(reilly)[1], replace=F)

  #overlap expression and atac:
  reilly01 <- table(!is.na(match(reillySig, genesDE01)))[2]
  reilly05 <- table(!is.na(match(reillySig, genesDE05)))[2]
  reilly10 <- table(!is.na(match(reillySig, genesDE10)))[2]

  permutation.stats <- rbind(permutation.stats, cbind(reilly01, reilly05, reilly10))
}

write.table(permutation.stats, file="../reilly_et_al_2015_permutation_tests.txt", quote=F, col.names=T, row.names=F)

dim(permutation.stats[permutation.stats$reilly10 >= length(which(loessSig10$genes %in% reilly$V1)),])
dim(permutation.stats[permutation.stats$reilly05 >= length(which(loessSig05$genes %in% reilly$V1)),])
dim(permutation.stats[permutation.stats$reilly01 >= length(which(loessSig01$genes %in% reilly$V1)),])


###################################################################
### 10. EXAMINE SOURCES OF VARIATION, LOOK AT GENES SEPARATELY. ###
###################################################################

# For now this is copied directly from the variancePartition manual:

# Set up multithreading:
library(doParallel) 
cl <- makeCluster(2) # Or the number of cores you have requested
registerDoParallel(cl)

# It seems all categorical variables are modelled as random effects here; it throws a warning if I try it any other way and refuses to work... so 
#varParModel <- ~ species + sex + (1|ind)
varParModelAllRandom <- ~ (1|species) + (1|sex) + (1|ind)

varPartFitting <- fitExtractVarPartModel( rpkmNormLoess, varParModelAllRandom, samplesMeta )

# sort variables (i.e. columns) by median fraction of variance explained
vp <- sortCols( varPartFitting )

# Some basic plots:
pdf(file="eda_plots/variance_partition_top100_genes_varPart.drop_inds.pdf")
plotPercentBars( vp[1:100,] )
dev.off()

pdf(file="eda_plots/variance_partition_violinplots_varPart.drop_inds.pdf")
plotVarPart( vp )
dev.off()

# These are interesting results! They suggest just as much of the variation is explained by the individuals as it is by the species, which is worrisome... so now let's break this down a bit more, and look more closely at the data. But it's also suggesting that these types of data are going to be inherently noisy. :(

# Or is it driven by some individual replicates and not all? Let's look in more detail
# Again, back to variancePartition manual:

# specify formula to model within/between individual variance separately for each tissue
# Note that including +0 ensures each tissue is modeled explicitly # Otherwise, the first tissue would be used as baseline
varPartModelSpecies <- ~ (species+0|ind) + (1|sex)
# fit model and extract variance percents
varPartFittingSpecies <- fitExtractVarPartModel( rpkmNormLoess, varPartModelSpecies, samplesMeta )
vpSpecies <- sortCols( varPartFittingSpecies )

# Some basic plots:
pdf(file="eda_plots/variance_partition_top100_genes_varPartSpecies.drop_inds.pdf")
plotPercentBars( vpSpecies[1:100,] )
dev.off()

pdf(file="eda_plots/variance_partition_violinplots_varPartSpecies.drop_inds.pdf")
plotVarPart( vpSpecies )
dev.off()

varPartModelInds <- ~ (ind+0|species) + (1|sex)
# fit model and extract variance percents
varPartFittingInds <- fitExtractVarPartModel( rpkmNormLoess, varPartModelInds, samplesMeta )
vpInds <- sortCols( varPartFittingInds )

# Some basic plots:
pdf(file="eda_plots/variance_partition_top50_genes_varPartInds.drop_inds.pdf")
plotPercentBars( vpInds[1:50,] )
dev.off()

pdf(file="eda_plots/variance_partition_violinplots_varPartInds.drop_inds.pdf")
plotVarPart( vpInds )
dev.off()




