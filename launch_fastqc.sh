#!/bin/bash

# IGR 16/07/25
# File: launch_fastqc.sh

# Launches fastqc for all files and then flags down problematic ones by writing a little report. 

# $1: Path to raw reads (gzipped fastq files)
# $2: Output directory name

if [ $# -ne 2 ]; then
    echo "USAGE: launch_fastqc.sh InputRawDirectory OutputDirectory"
    exit 1
fi

fastqdirectory=$1
outdir=$2

# Create directories for the output
if [ ! -d $outdir ]; then
    mkdir -p $outdir
elif [ ! -d $outdir/logs ]; then
    mkdir -p $outdir/logs
fi

## Get the samples and launch the job - very straight forward.
for tsampName in `ls $fastqdirectory/*.gz`; do
    sampName=`basename $tsampName` 
    sampNameSlim=`basename $tsampName .gz`
    echo "$sampName"

    ## Generate the qsub script inline
    echo -e "#! /bin/bash

    echo 'Launching fastqc for sample \$sampName'
    fastqc --extract --outdir $outdir $fastqdirectory/$sampName  
    grep \"FAIL\\\|WARN\" $outdir/$sampNameSlim_fastqc/summary.txt >> $outdir/all_samples_report.txt

    exitstatus=\$?
    if [ \$exitstatus != 0 ]; then 
        echo ERROR: Something went wrong with the fastqc job and it failed with exit code \$exitstatus.
        exit \$exitstatus
    else
        echo Succesfully ran and parsed fastqc for sample $sampName.
    fi
" | qsub -N $sampName.qc.filter -o $outdir/logs/$sampName.qc.filter.out -l mem=4gb,walltime=1:00:00 -M igr@ntu.edu.sg -m ae -V -j oe
done

