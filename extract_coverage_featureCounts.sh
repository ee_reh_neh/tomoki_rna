#!/bin/bash

# IGR 16/08/10
# File: extract_coverage_featureCounts.sh

# Launches featureCounts for all files, separately by species, generates matrices of results both clean and dirty. 

# $1: Path to mapped reads (gzipped fastq files)
# $2: Output directory name

if [ $# -ne 3 ]; then
    echo "USAGE: extract_coverage_featureCounts.sh InputRawDirectory OutputDirectory TopHatType"
    echo "TopHatType: first, none, alt"
    exit 1
fi

bamdirectory=$1
outdir=$2
tophat=$3

# Location of reference file roots:
macFasOrtho="/vlsci/SG0008/igallego/orthoexon/hg38v86_macFas5_ortho/macFas5_ensembl86_orthoexon_"
humanOrtho="/vlsci/SG0008/igallego/orthoexon/hg38v86_macFas5_ortho/hg38_ensembl86_orthoexon_"

# Create directories for the output
if [ ! -d $outdir/logs ]; then
    mkdir -p $outdir/logs
fi

## Parse the samples by species:
for longList in `ls $bamdirectory`; do
    sampName=`basename $longList`
    if echo $sampName | grep -q "^M"; then
        macFasList="$bamdirectory/${sampName}/accepted_hits.bam $macFasList"
    elif echo $sampName | grep -q "^H\|^E"; then
        humanList="$bamdirectory/${sampName}/accepted_hits.bam $humanList"
    fi    
done

# Launch featurecounts for either species at the multiple thresholds
# This loop can be commented out when the time comes. 
for i in 0.92; do 
    for j in 2; do
        for k in FALSE; do    

            # Macaques:
            echo -e "#! /bin/bash

            echo 'Launching featureCounts for all macFas samples.'
            if [ -s $outdir/macFas_counts_matrix_${i}.out ]; then
                echo 'featureCounts has already been run for this species.'
            else
                # Launch featureCounts
                # Building the command:
                # -T: number of threads, set to 8 by me. 
                # -F: format of the feature file; set to SAF. See the manual for format specification. File is generated automatically by blat_processing_intraspecies.R
                # -a: annotation file name - here, species-specific ortho exon file.
                # -s: assign strandedness to data set. Set to 1, first strand, on the basis of library construction. 
                # -o: sets output file name

                echo \" featureCounts -T 8 -F SAF -a ${macFasOrtho}${i}_ortho_${k}_pc.txt -s ${j} -o $outdir/macFas_${i}pc_th_${tophat}_fc_${j}_ortho_${k}.out $macFasList \"        
                featureCounts -T 8 -F SAF -a ${macFasOrtho}${i}_ortho_${k}_pc.txt -s ${j} -o $outdir/macFas_${i}pc_th_${tophat}_fc_${j}_ortho_${k}.out $macFasList 
                # Clean up output for R
                sed 's:\/::g' $outdir/macFas_${i}pc_th_${tophat}_fc_${j}_ortho_${k}.out | sed 's/vlsciSG0008igallegotomokimapped_datafirst_strand//g' | sed 's/.gzaccepted_hits\.bam//g' > $outdir/macFas_${i}pc_th_${tophat}_fc_${j}_ortho_${k}_clean.out

                exitstatus=( \$? )
                if [ \${exitstatus}  != 0 ]; then
                    echo ERROR: Failed featureCounts with exit code \${exitstatus}
                    exit \${exitstatus}
                else 
                    echo Successfully ran featureCounts for all macFas samples.
                fi
            fi
            " | sbatch --time=00:5:00 --mem=4096 --cpus-per-task=2 -p sysgen --account=SG0008 --mail-type=FAIL,END --output $outdir/logs/macFas_${i}pc_${j}_${k}_${tophat}.out --job-name=fcMacFas$i$tophat$j$k --export=ALL

            # Humans
            echo -e "#! /bin/bash

            echo 'Launching featureCounts for all human samples.'
            if [ -s $outdir/human_${i}pc_th_first_fc_first.out ]; then
                echo 'featureCounts has already been run for this species.'
            else
                # Launch featureCounts
                # Building the command:
                # -T: number of threads, set to 8 by me. 
                # -F: format of the feature file; set to SAF. See the manual for format specification. File is generated automatically by blat_processing_intraspecies.R
                # -a: annotation file name - here, species-specific ortho exon file.
                # -s: assign strandedness to data set. Set to 1, first strand, on the basis of library construction. 
                # -o: sets output file name
                
                echo \" featureCounts -T 8 -F SAF -a ${humanOrtho}${i}_ortho_${k}_pc.txt -s ${j} -o $outdir/human_${i}pc_th_${tophat}_fc_${j}_ortho_${k}.out $humanList \"        
                featureCounts -T 8 -F SAF -a ${humanOrtho}${i}_ortho_${k}_pc.txt -s ${j} -o $outdir/human_${i}pc_th_${tophat}_fc_${j}_ortho_${k}.out $humanList 
                # Clean up output for R
                sed 's:\/::g' $outdir/human_${i}pc_th_${tophat}_fc_${j}_ortho_${k}.out | sed 's/vlsciSG0008igallegotomokimapped_datafirst_strand//g' | sed 's/.gzaccepted_hits\.bam//g' > $outdir/human_${i}pc_th_${tophat}_fc_${j}_ortho_${k}_clean.out

                exitstatus=( \$? )
                if [ \${exitstatus}  != 0 ]; then
                    echo ERROR: Failed featureCounts with exit code \${exitstatus}
                    exit \${exitstatus}
                else 
                    echo Successfully ran featureCounts for all human samples.
                fi
            fi
            " | sbatch --time=00:5:00 --mem=4096 --cpus-per-task=2 -p sysgen --account=SG0008 --mail-type=FAIL,END --output $outdir/logs/human_${i}pc_${j}_${k}_${tophat}.out --job-name=fchuman$i$tophat$j$k --export=ALL


        done
    done
done
