#!/bin/bash

# IGR 16/07/25
# File: map_and_clean.sh

# Launches tophat for all files, and then generates a brief report with summary statistics for each of them.
# This is followed by using picard to merge different lanes.
# Based on a script by Julien Roux (launch_tophat.pl)

### THIS IS AN ALTERNATE VERSION OF THE REAL MAPPING SCRIPT (map_and_clean_alt_strand.sh)
### USED ONLY FOR CHECKING STRAND ASSIGNMENT OF THE READS
### IGR, 16/08/11

# $1: Path to raw reads (gzipped fastq files)
# $2: Output directory name

if [ $# -ne 2 ]; then
    echo "USAGE: map_and_clean.sh InputRawDirectory OutputDirectory"
    exit 1
fi

fastqdir=$1
outdir=$2

# Location of reference genomes:
macfas="/home/users/ntu/igr/reference/bowtie2_indexes/macFas5/macFas5_all"
hg38="/home/users/ntu/igr/reference/bowtie2_indexes/hg38/hg38_no_alt"

# Create directories for the output
if [ ! -d $outdir ]; then
    mkdir -p $outdir
fi
if [ ! -d $outdir/logs ]; then
    mkdir -p $outdir/logs
fi
if [ ! -d $fastqdir/cat ]; then
    mkdir -p $fastqdir/cat
fi

## Before launching the job, parse the samples:
    # Want to group lanes together before - otherwise coverage-dependent results will suffer if done in parallel and then merged together. 
    # Generate directory to output results:
    for tsampName in `ls $fastqdir/*001.fastq.gz`; do
        sampNameBase=`basename $tsampName | cut -f 1-2 -d_`
        # Merge lanes:
        if [ ! -f $fastqdir/cat/$sampNameBase.fastq.gz ]; then
            cat $fastqdir/${sampNameBase}_* > $fastqdir/cat/$sampNameBase.fastq.gz
        fi
    done

## To prevent launching two mapping jobs per sample, we start a new loop here:
    for tsampName in `ls $fastqdir/cat/*fastq.gz`; do
        sampNameBase=`basename $tsampName`

        # Check and assign species
        if echo $sampNameBase | grep -q "^M"; then
            refgenome=$macfas
            refname=macFas5_all
        elif echo $sampNameBase | grep -q "^H\|^E"; then
            refgenome=$hg38
            refname=hg38_no_alt
        fi    

        fastqtomap=$fastqdir/cat/$sampNameBase
        echo $fastqtomap

    ## Launch tophat and samtools
        echo -e "#! /bin/bash

        if [ -s $outdir/$sampNameBase/accepted_hits.bam ]; then 
            echo 'This sample has already been mapped by tophat. Moving ahead to summarising it with samtools.'
        else
            echo Launching topHat for sample $sampNameBase

            # The TopHat command, explained:
            # -o: output directory, specified above
            # -N: number of mismatches, 4 allowed (100 bp reads, can adjust at some other time if needed)
            # --read-edit-dist: 4, same as mismatches (what is the difference? Does this include indels?) 
            # --read-realign-edit-dist: From the tophat manual, \"Some of the reads spanning multiple exons may be mapped incorrectly as a contiguous alignment to the genome even though the correct alignment should be a spliced one - this can happen in the presence of processed pseudogenes that are rarely (if at all) transcribed or expressed. This option can direct TopHat to re-align reads for which the edit distance of an alignment obtained in a previous mapping step is above or equal to this option value.\" Edit distance set to 1 in this case, because I know mapping to pseudogenes can be a massive problem. Might have to be adjusted. 
            # -g: 1 - maximum number of hits allowed (no multi-mapping)
            # -p: Number of threads to use for mapping. Trying it with 8, because I can. According to Julien, no performance improvement between 4 and 8 that he could see, but these cores are newer and bigger, so...
            # -i: Minimum intron size, set to 70 by default - simply being explicitly declared.
            # -I: Maximum intron size, set to 50000 by default - simply being explicity declared. 
            # --b2-fast: The sensitivity of bowtie2 for the mapping - set to fast rather than very fast to catch some of the finer problems, might adjust if runtime becomes too long. From the bowtie2 manual, equivalent to \"-D 10 -R 2 -N 0 -L 22 -i S,0,2.50\"
            # -m: Number of mismatches allowed in the splice site anchor region; conservatively set to 0 to begin with (default value)
            # --bowtie-n: Controls bowtie2 behaviour for initial mapping. Rather than the default -v mode, the -n flag is quality-aware, such that mismatches in high-quality bases are penalised higher than those in low-quality bases. In cases where there are two mappings with the same quality, the one where mismatches are in lower-quality bases will be retained over the one with high-quality mismatches. 
            # --no-coverage-search: calls junctions on the basis of coverage of unannotated islands. Disabled for large reads and large # of reads at the suggestion of the manual.
            # --library-type: Type of library generated. fr-secondstrand FOR TESTING PURPOSES ONLY. 
        

            tophat -o $outdir/$sampNameBase -N 4 --read-edit-dist 4 --read-realign-edit-dist 1 -g 1 -p 8 -i 70 -I 50000 --b2-fast -m 0 --bowtie-n --no-coverage-search --library-type fr-secondstrand $refgenome $fastqtomap

            exitstatus=\$?
            if [ \$exitstatus != 0 ]; then 
                echo ERROR: Tophat mappping for sample $sampNameBase failed with error code \$exitstatus.
                exit \$exitstatus
            else
                echo Succesfully mapped $sampNameBase.
            fi
        fi

        ## Launch SAMtools to write report
        if [ -s $outdir/$sampNameBase/samtools_report.txt ]; then 
            echo 'This sample has already been summarised with samtools.'
        else
            echo 'Summarising sample \$sampName'

            outdirLong=\`$outdir/$sampNameBase\`
            samtools view $outdir/$sampNameBase/accepted_hits.bam | perl $scriptsdir/get_stats_piped.pl \$outdirLong $sampNameBase        

            exitstatus=\$?
            if [ \$exitstatus != 0 ]; then 
                echo ERROR: Samtools analysis for sample $sampNameBase failed with error code \$exitstatus.
                exit \$exitstatus
            else
                echo Succesfully summarised $sampName.
            fi
        fi 
        "| qsub -l walltime=24:00:00,mem=20gb,ncpus=8 -V -o $outdir/logs/${sampNameBase}_tophat.log -N th.$sampNameBase -M igr@ntu.edu.sg -m ae -j oe

    done