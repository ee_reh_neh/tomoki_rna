### expression_eda.R
### For experimental analysis of RNA-seq data from human and crab-eating macaque neural progenitors. 
### Modelled closely on main_analysis_final.R from IGR's chimp-human iPSC paper
### IGR 16.08.14

### 0. PREPARE WORKSPACE. ###
### 1. READ IN COUNT MATRICES AND EXON LISTS. ### 
### 2. CALCULATE NORMALISED LIBRARY SIZES, COMPUTE CPM, FILTER, LOESS NORMALISE, AND COMPUTE RPKM. ###
### 3. DIFFERENTIAL EXPRESSION TESTING. ###
### 4. LOTS OF PLOTS TO ASSESS GOODNESS OF FIT AND LOOK AT DE GENES. ###
### 5. ANNOTATE TOP GENES AND DO BASIC GO TESTING. ###
### 6. GO TESTING WITH GOSEQ INSTEAD OF TOPGO. ###

#############################
### 0. PREPARE WORKSPACE. ###
#############################

#extraVars <- commandArgs(trailingOnly=T)
extraVars <- c("ortho_FALSE", 0.92) 

### load libraries
library("gplots")
library(RColorBrewer)
library(limma)
library(edgeR)
library(statmod)
library(beeswarm)
library(biomaRt)
library(topGO)
library(SparseM)
library(org.Hs.eg.db)
library(goseq)
library(qvalue)
library(plyr)
library(VennDiagram)

options(width=200)
# setwd("~/tomoki/rpkm/ensembl86")
setwd("~/cloudstor/Data/tomoki/second_batch/")
pal <- c(brewer.pal(, "BuPu"), "black", brewer.pal(6, "YlOrRd"))

#################################################
### 1. READ IN COUNT MATRICES AND EXON LISTS. ### 
#################################################


hsOrthoCounts <- read.table(paste0("human_", extraVars[2], "pc_th_first_fc_2_", extraVars[1], "_clean.out"), header=T)
mfOrthoCounts <- read.table(paste0("macFas_", extraVars[2], "pc_th_first_fc_2_", extraVars[1], "_clean.out"), header=T)

mergedCounts <- merge(hsOrthoCounts[,c(1,7:17)], mfOrthoCounts[,c(1,7:15)], by.x="Geneid", by.y="Geneid", all=T)
names(mergedCounts) <- gsub("\\.fastq","", names(mergedCounts))
dim(mergedCounts)
# [1] 34142    21

# Remove unwanted individiuals - the failed individual with very very few reads overall: 
mergedCounts <- mergedCounts[,colnames(mergedCounts) != "EDI2.8_S5"]
mergedCounts <- mergedCounts[,!grepl("MFTW1|H9.2|H9.5", colnames(mergedCounts))]

# Fix the EDI names and convert to EDI2:
names(mergedCounts) <- gsub("EDI\\.", "EDI2\\.", names(mergedCounts))

### OPTIONAL THINGS TO DO:
    # Filter all ribosomal protein genes because they are terrible:
    ensembl <- useMart("ENSEMBL_MART_ENSEMBL", dataset="hsapiens_gene_ensembl", host="oct2016.archive.ensembl.org") # Ensembl 86, for continuity.
    geneFamilies <- getBM(attributes=c("ensembl_gene_id", "family", "family_description", "external_gene_name"), mart=ensembl)
    badRibosomes <- geneFamilies[grepl("MRP|RPS|RPL|RP L|RP S", geneFamilies$family_description),1] # Filtering genes associated with nasty ribosomal families - I wonder how effective this will be downstream!
    moreBadRibos <- geneFamilies[grepl("MRP|RPS|RPL|RP L|RP S", geneFamilies$external_gene_name),1]

    allBadRibos <- c(badRibosomes, moreBadRibos, "ENSG00000104517") # This final gene used to get filtered_no_fail.out, and as of 2016.12.20 no longer does - I don't understand why either before or after, but to keep it consistent I will simply exclude it manually. 

    mergedCounts <- mergedCounts[!mergedCounts$Geneid %in% allBadRibos,]
    dim(mergedCounts)
    # [1]  33669    15

    # For RPKM calculation later
    mergedCounts$hsLength <- hsOrthoCounts[!hsOrthoCounts$Geneid %in% allBadRibos,6]
    mergedCounts$mfLength <- mfOrthoCounts[!mfOrthoCounts$Geneid %in% allBadRibos,6]

    # drop non-autosomal genes
    # ychr.drop <- c("ENSG00000183878", "ENSG00000198692", "ENSG00000234652", "ENSG00000243576", "ENSG00000252472")
    # gene.lengths <- gene.lengths[!gene.lengths$GeneEnsemblID %in% ychr.drop,]

#prepare meta information from sample names:
countsNames <- colnames(mergedCounts[,2:15])
speciesCol <- c(rep("darkorchid4", 8), rep("orange", 6))
samplesMeta <- data.frame(countsNames, speciesCol)
names(samplesMeta) <- c("line", "col")
samplesMeta$cex <- 1.5

    #alternative colours etc:
    samplesMeta$species <- ifelse(grepl("^M", samplesMeta$line), "Mf", "Hs")
    samplesMeta$ind <- ifelse(grepl("^MF12", samplesMeta$line), "Mf12", ifelse(grepl("^MFTW", samplesMeta$line), "MfTW1", ifelse(grepl("^MF1", samplesMeta$line), "MF1", ifelse(grepl("^EDI2", samplesMeta$line), "EDI2", ifelse(grepl("^EDI", samplesMeta$line), "EDI", "H9")))))
    samplesMeta$sex <- ifelse(grepl("^MF12", samplesMeta$line), "M", "F")
    samplesMeta$indPch <- 14 + as.numeric(as.factor(samplesMeta$ind))

    indPal <- brewer.pal(6, "Dark2")
    samplesMeta$indPal <- indPal[as.numeric(as.factor(samplesMeta$ind))]

#Finally, add info on the covariates
covariates <- read.table("../sample_covariates.txt", header=T)
covariates$sample <- gsub("\\.fastq\\.gz","", covariates$sample)
covariates$sample <- gsub("-","\\.", covariates$sample)
names(covariates)[1] <- "line" # Need to rename for join to work in the next line. 
samplesMeta <- plyr::join(samplesMeta, covariates) 

# save(samplesMeta, file="samples_meta_expression_final.Rda")

######################################################################################################
### 2. CALCULATE NORMALISED LIBRARY SIZES, COMPUTE CPM, FILTER, LOESS NORMALISE, AND COMPUTE RPKM. ###
######################################################################################################

print("Going from raw counts to RPKM:")

# reads into edgeR, calculate TMM and then CPM, writing_no_fail.out the intermediate steps:
mergedCountsDge <- DGEList(counts=as.matrix(mergedCounts[,2:15]), genes=mergedCounts[,1])
mergedCountsDge <- calcNormFactors(mergedCountsDge)
#save(mergedCountsDge, file="ipsc_genes_dge_ipsc_final.Rda")
#write.table(mergedCountsDge$samples, file="TMM.normFactors_ipsc_genes_ipsc_final.txt", sep="\t", quote=F, row.names=T)

### Calculate log2 CPM
cpmNorm <- cpm(mergedCountsDge, normalized.lib.sizes=TRUE, log=TRUE, prior.count=0.25) ### I may come back and try this with the Irizarry prior of 0.5, because it makes more intuitive sense to me... but this one is simply -2, so really, no difference
pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_cpm.density_ipsc_clean_no_fail.pdf"))
plotDensities(cpmNorm, group=samplesMeta$species) 
abline(v=1)
dev.off()

# Filter on observing cpm greater or equal to 1 or more in at least half of the individuals in one species, not keeping library sizes.
mergedCountsDgeFilt <- mergedCountsDge[rowSums(cpmNorm[,1:8] >= 2) >= 4 | rowSums(cpmNorm[,9:14] >= 2) >= 3 , , keep.lib.sizes=F] 
dim(mergedCountsDgeFilt)
#[1] 11051    14

pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_cpmFilt.density_ipsc_clean_no_fail.pdf"))
plotDensities(mergedCountsDgeFilt, group=samplesMeta$species)
dev.off()

# Recalculate TMM and lib sizes
mergedCountsDgeFilt <- calcNormFactors(mergedCountsDgeFilt) ## recalculate norm factors
# save(mergedCountsDgeFilt, file="mergedCountsDge.TMMFilt_clean.RDa")

# Loess normalise the data
## For starters, Voom requires a design matrix as input
design <- model.matrix(~ 0 + samplesMeta$species + as.factor(samplesMeta$sex) + as.factor(samplesMeta$seqBatch))
colnames(design) <- c("human", "macFas", "sex", "seqBatch2", "seqBatch3")

## Voom on filtered nonGC normalized data, with cyclic loess normalization, and blocked by individual replicates - this requires two passes, one wit_no_fail.out the random individual effect and a second one that takes it into account - see the limma manual and this post and reply by Gordon Smyth!
## https://support.bioconductor.org/p/59700/

## note that cyclic loess is designed for between-array normalisation rather than RNA-seq, but it should still be usable. Ideally we would not need it, however, but I don't like the directionality of the results wit_no_fail.out normalisation, not to mention there's a clear_no_fail.outlier sample in there. 
cpmNormLoess <- voom(mergedCountsDgeFilt, design, normalize.method="cyclicloess", plot=F) 
cpmCorfit <- duplicateCorrelation(cpmNormLoess, design, block=samplesMeta$ind)
    #Gives six warnings with final data set
    cpmCorfit$consensus
# [1] 0.5169432
pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_voom.loess.cpm.indrandom_no_fail.output_clean_no_fail.pdf"))
cpmNormLoess <- voom(mergedCountsDgeFilt, design, normalize.method="cyclicloess", plot=T, correlation=cpmCorfit$consensus, block=samplesMeta$ind) 
dev.off()

# Second round of duplicate correlations, as recommended by Gordon Smyth. 
cpmCorfit <- duplicateCorrelation(cpmNormLoess, design, block=samplesMeta$ind)
    #Gives eight warnings with final data set. 
cpmCorfit$consensus
# [1] 0.5276686

pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_voom.loess.cpm.indrandom.density_clean_no_fail.pdf"))
plotDensities(cpmNormLoess, group=samplesMeta$species) 
dev.off()

## Turn this into RPKM
# Values are already log2 CPM, so just need to substract
rpkmNormLoess <- cpmNormLoess
rpkmNormLoess$E[,1:8] <- cpmNormLoess$E[,1:8] - log2((mergedCounts[rowSums(cpmNorm[,1:8] > 2) >= 4 | rowSums(cpmNorm[,9:14] > 2) >= 3 , 16])/1000)
rpkmNormLoess$E[,9:14] <- cpmNormLoess$E[,9:14] - log2((mergedCounts[rowSums(cpmNorm[,1:8] >= 2) >= 4 | rowSums(cpmNorm[,9:14] >= 2) >= 3 , 17])/1000)

rpkmsForWrite <- data.frame(rpkmNormLoess$genes, rpkmNormLoess$E)
write.table(rpkmsForWrite, file=paste0(extraVars[1], "_", extraVars[2], "_voom.loess.rpkm.indrandom_no_fail.out"), col.names=T, row.names=F, quote=F, sep="\t", eol="\n")

pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_voom.loess.rpkm.indrandom.density_clean_no_fail.pdf"))
plotDensities(rpkmNormLoess, group=samplesMeta$species) #again, a beautiful distribution, as expected. 
dev.off()


### Same, but explicitly modelling sex:
    # LoessSex normalise the data
    ## For starters, Voom requires a design matrix as input
    designSex <- model.matrix(~ 0 + samplesMeta$species + as.factor(samplesMeta$sex) + as.factor(samplesMeta$seqBatch))
    colnames(designSex) <- c("human", "macFas", "sex", "seqBatch2", "seqBatch3")

    ## Voom on filtered nonGC normalized data, with cyclic loess normalization, and blocked by individual replicates - this requires two passes, one without the random individual effect and a second one that takes it into account - see the limma manual and this post and reply by Gordon Smyth!
    ## https://support.bioconductor.org/p/59700/

    ## note that cyclic loess is designSexed for between-array normalisation rather than RNA-seq, but it should still be usable. Ideally we would not need it, however, but I don't like the directionality of the results without normalisation, not to mention there's a clear outlier sample in there. 
    cpmNormLoessSex <- voom(mergedCountsDgeFilt, designSex, normalize.method="cyclicloess", plot=F) 
    print("Loess normalisation duplicate correlation (yes sex model), first pass:")
    cpmCorfitSex <- duplicateCorrelation(cpmNormLoessSex, designSex, block=samplesMeta$ind)
        cpmCorfitSex$consensus
    cpmNormLoessSex <- voom(mergedCountsDgeFilt, designSex, normalize.method="cyclicloess", correlation=cpmCorfitSex$consensus, block=samplesMeta$ind, plot=F) 
 
    # Second round of duplicate correlations, as recommended by Gordon Smyth. 
    print("Loess normalisation duplicate correlation (yes sex model), first pass:")
    cpmCorfitSex <- duplicateCorrelation(cpmNormLoessSex, designSex, block=samplesMeta$ind)
    cpmCorfitSex$consensus
    # [1] 0.3295204

    ## Turn this into RPKM
    # Values are already log2 CPM, so just need to substract
    rpkmNormLoessSex <- cpmNormLoessSex
    rpkmNormLoessSex$E[,1:8] <- cpmNormLoessSex$E[,1:8] - log2((mergedCounts[rowSums(cpmNorm[,1:8] >= 2) >= 4 | rowSums(cpmNorm[,9:14] >= 2) >= 3 , 16])/1000)
    rpkmNormLoessSex$E[,9:14] <- cpmNormLoessSex$E[,9:14] - log2((mergedCounts[rowSums(cpmNorm[,1:8] >= 2) >= 4 | rowSums(cpmNorm[,9:14] >= 2) >= 3 , 17])/1000)

###########################################
### 3. DIFFERENTIAL EXPRESSION TESTING. ###
###########################################

print("Differential expression testing:")

## Loess norm with individual blocking/random effect
rpkmNormLoessFit <- lmFit(rpkmNormLoess, design, block=samplesMeta$ind, correlation=cpmCorfit$consensus)

contrasts <- makeContrasts(
                HumanvsMacFas = (human - macFas),
                levels=design)

rpkmNormLoessFit2 <- contrasts.fit(rpkmNormLoessFit, contrasts)
rpkmNormLoessFit2 <- eBayes(rpkmNormLoessFit2)

topGenesLoess <- topTable(rpkmNormLoessFit2, adjust="BH", number=Inf, sort.by="p")
print("DE at FDR 1%, no sex")
table(decideTests(rpkmNormLoessFit2, adjust.method="BH", method="separate", p.value=0.01))

print("DE at FDR 5%, no sex")
table(decideTests(rpkmNormLoessFit2, adjust.method="BH", method="separate", p.value=0.05))

print("DE at FDR 10%, no sex")
table(decideTests(rpkmNormLoessFit2, adjust.method="BH", method="separate", p.value=0.10))

write.table(topGenesLoess, file=paste0(extraVars[1], "_", extraVars[2], "_voom.loess.cpm.indrandom.de_testing_results_no_fail.out"), quote=F, row.names=F, col.names=T, sep="\t", eol="\n")

## Loess norm with individual blocking/random effect AND SEX
    rpkmNormLoessSexFit <- lmFit(rpkmNormLoessSex, designSex, block=samplesMeta$ind, correlation=cpmCorfitSex$consensus)

    contrastsSex <- makeContrasts(
                    HumanvsMacFas = (human - macFas),
                    levels=designSex)

    rpkmNormLoessSexFit2 <- contrasts.fit(rpkmNormLoessSexFit, contrastsSex)
    rpkmNormLoessSexFit2 <- eBayes(rpkmNormLoessSexFit2)

    topGenesLoessSex <- topTable(rpkmNormLoessSexFit2, adjust="BH", number=Inf, sort.by="p")
    print("DE at FDR 1%, yes sex")
    table(decideTests(rpkmNormLoessSexFit2, adjust.method="BH", method="separate", p.value=0.01))

    print("DE at FDR 5%, yes sex")
    table(decideTests(rpkmNormLoessSexFit2, adjust.method="BH", method="separate", p.value=0.05))

    print("DE at FDR 10%, yes sex")
    table(decideTests(rpkmNormLoessSexFit2, adjust.method="BH", method="separate", p.value=0.10))

    write.table(topGenesLoessSex, file=paste0(extraVars[1], "_", extraVars[2], "_voom.loess.cpm.indrandom.sex.de_testing_results_no_fail.out"), quote=F, row.names=F, col.names=T, sep="\t", eol="\n")

########################################################################
### 4. LOTS OF PLOTS TO ASSESS GOODNESS OF FIT AND LOOK AT DE GENES. ###
########################################################################

print("Making pretty plots.")


#Define the new plotting functions:
# The one that plots individual gene values:
check_results <- function(count_table, top, n) {
  cols <- colorRampPalette(pal)(n)
  rownames(count_table$E) <- count_table$genes$genes
  plot(1,1, xlim=c(1, length(count_table[1,])), ylim=c(min(count_table[rownames(count_table) %in% top$genes[1:n],]$E), max(count_table[rownames(count_table) %in% top$genes[1:n],]$E)), type="n", xaxt="n", ylab="Normalized expression", xlab="")
  text(1:length(count_table[1,]), min(count_table[rownames(count_table) %in% top$genes[1:n],]$E)-0.07*(max(count_table[rownames(count_table) %in% top$genes[1:n],]$E)-min(count_table[rownames(count_table) %in% top$genes[1:n],]$E)), srt = 45, adj = 1,labels = colnames(count_table), xpd = TRUE)
  axis(side=1, at=c(1:length(count_table[1,])), labels=rep("", dim(count_table$targets)[1]))
  abline(v=1:length(count_table[1,]), lty=3, col="lightgray")
 
  if (n > length(top[,1])){
    n <- length(top[,1])
  }
  for (i in 1:n){
    lines(1:length(count_table[1,]), count_table[rownames(count_table) %in% top$genes[i],]$E, col=cols[i])
   }
}

# The volcano plot:
volcano.plot <- function(gene.list, significance, outfile){
    pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_volcano_", outfile, "_no_fail.pdf"))
    plot(gene.list$logFC, -log10(gene.list$adj.P.Val), xlab="log2 fold change", ylab="-log10 corrected P value", main=outfile, cex=0.75, pch=16, cex.lab=1.5, cex.axis=1.3, col=rgb(0,0,0,.75))
    abline(v = 0, lty=2, col=adjustcolor("red", alpha=0.8), lwd=2.5)
    abline(h = -log10(significance), lty=2, col=adjustcolor("red", alpha=0.8), lwd=2.5)
    legend(x = "topleft", paste(dim(gene.list[gene.list$adj.P.Val < significance & gene.list$logFC < 0,])[1], " genes\n(", dim(gene.list[gene.list$adj.P.Val < significance & gene.list$logFC < -1,])[1], " FC < -1)", sep=""), bty="n", cex=1.3)
    legend(x = "topright", paste(dim(gene.list[gene.list$adj.P.Val < significance & gene.list$logFC > 0,])[1], " genes\n(", dim(gene.list[gene.list$adj.P.Val < significance & gene.list$logFC > 1,])[1], " FC > 1)", sep=""), bty="n", cex=1.3)
    legend(x = "bottomleft", paste(dim(gene.list[gene.list$adj.P.Val > significance & gene.list$logFC < 0,])[1], " genes\n(", dim(gene.list[gene.list$adj.P.Val > significance & gene.list$logFC < -1,])[1], " FC < -1)", sep=""), bty="n", cex=1.3)
    legend(x = "bottomright", paste(dim(gene.list[gene.list$adj.P.Val > significance & gene.list$logFC > 0,])[1], " genes\n(", dim(gene.list[gene.list$adj.P.Val > significance & gene.list$logFC > 1,])[1], " FC > 1)", sep=""), bty="n", cex=1.3)
    dev.off()   
}

# Define the number of genes to plot:
gene.plots <- c(10,50,100,200,500,1000)
colors <- rev(colorRampPalette(brewer.pal(10,"RdBu"))(100)) ## We want red to be higher expression and blue lower

# volcano.plot(topGenes, 0.01, "all_genes_fdr_0.01")
# volcano.plot(topGenes, 0.05, "all_genes_fdr_0.05")
# volcano.plot(topGenes, 0.10, "all_genes_fdr_0.10")

# The actual plotting function, which makes all kinds of plots:

makeAllPlots <- function(deObject, fitObject, exprsObject, topTable, normName){
    # Model goodness plots:
    pdf(paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_voom.", normName, ".indblock.maplot_no_fail.pdf"))
    par(mfrow=c(1,2))
    limma::plotMA(deObject, coef=1, xlab="average coefficient", ylab="estimated coefficient", main="human") ## for a bunch of exons with medium intensity (-2<cpm<2), in chimp heart they are either highly expressed or not expressed. Highly expressed genes (on average) seem to be less variable in individual samples
    abline(a=0, b=1,col=pal[1])
    limma::plotMA(deObject, coef=2, xlab="average coefficient", ylab="estimated coefficient", main="macFas")
    abline(a=0, b=1,col=pal[1])
    dev.off()

    # MA plot:
    pdf(paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_voom.", normName, ".indblock.mdplot_no_fail.pdf"))
    plotMD(fitObject, coef=1, xlab="average expression", ylab="log2 FC")
    abline(h=0, col="red", lty=1, lwd=1)
    dev.off()

    ## Sigma vs A plot. After a linear model is fitted, this checks constancy of the variance with respect to intensity level.
    pdf(paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_voom.", normName, ".indblock.sigmaaplot_no_fail.pdf"))
    plotSA(deObject, main="species")
    dev.off()

    ## Boxplot of the residuals (difference between the true value and fitted value)
    pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_voom.", normName, ".indblock.residuals_no_fail.pdf"))
    hist(log2(deObject$sigma), breaks=100)
    boxplot(log2(deObject$sigma), names="species")
    dev.off()

    # DE plots
    # for (i in 1:6){
    #     pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_voom.", normName, ".indblock.check_results_top_", gene.plots[i], "_no_fail.pdf"))
    #     check_results(exprsObject, topTable, gene.plots[i])
    #     dev.off()

    #    pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_voom.", normName, ".indblock.heatmap_top_", gene.plots[i], "_no_fail.pdf"))
    #     heatmap.2(exprsObject$E[rownames(topTable[1:gene.plots[i],]),], col = colors, margins = c(12, 12), trace='none', denscol="white", labCol=samplesMeta$species, labRow=NA, ColSideColors=pal[as.integer(as.factor(samplesMeta$species))], cexCol = 1.5)
    #     dev.off()
    # }

    volcano.plot(topTable, 0.01, paste0(normName, "_all_genes_fdr_0.01"))
    volcano.plot(topTable, 0.05, paste0(normName, "_all_genes_fdr_0.05"))
    volcano.plot(topTable, 0.10, paste0(normName, "_all_genes_fdr_0.10"))
}

makeAllPlots(rpkmNormLoessFit, rpkmNormLoessFit2, rpkmNormLoess, topGenesLoess, "loess_clean")
makeAllPlots(rpkmNormLoessSexFit, rpkmNormLoessSexFit2, rpkmNormLoessSex, topGenesLoessSex, "loess_sex_clean")


######################################################
### 5. ANNOTATE TOP GENES AND DO BASIC GO TESTING. ###
######################################################

print("Testing for GO category enrichments using topGO.")

### define the testing function
de.testing <- function(gene.in, universe, ontology, suffix, algorithm){
    #pull the genes that are significant
    de.all <- ifelse(universe %in% gene.in, 1, 0)
    names(de.all) <- universe
    de.all <- as.factor (de.all)

    #test for DE: all
    for (i in 1){
        test.degenes <- new("topGOdata", ontology=ontology, allGenes=de.all, nodeSize = 5, annot = annFUN.org, mapping = "org.Hs.eg.db", ID = "ensembl")
        test.degenes.fisher <- runTest(test.degenes, algorithm = algorithm, statistic = "fisher")
        test.degenes.out <- GenTable(test.degenes, classicFisher = test.degenes.fisher, orderBy = "classicFisher", ranksOf = "classicFisher", topNodes = length(score(test.degenes.fisher)))
    
        #fix p-values
        test.degenes.out$BH <- p.adjust(test.degenes.out$classicFisher, method="BH")
         test.degenes.out$fisher.fix <- as.numeric(test.degenes.out$classicFisher)
         test.degenes.out[is.na(test.degenes.out$fisher.fix),8] <- 10^-30
#         test.degenes.out$qvalue <- qvalue(test.degenes.out$fisher.fix)$qvalue
       
        #and collecting summary stats
#        test.stats <- c("all", suffix, ontology, algorithm, length(gene.in), test.degenes.fisher@geneData[1], test.degenes.fisher@geneData[2], dim(test.degenes.out)[1], dim(test.degenes.out[test.degenes.out$classicFisher < 0.05,])[1],dim(test.degenes.out[test.degenes.out$BH < 0.05 | is.na(test.degenes.out$BH),])[1], dim(test.degenes.out[test.degenes.out$qvalue < 0.05,])[1])
        test.stats <- c("all", suffix, ontology, algorithm, length(gene.in), test.degenes.fisher@geneData[1], test.degenes.fisher@geneData[2], dim(test.degenes.out)[1], dim(test.degenes.out[test.degenes.out$classicFisher < 0.05,])[1],dim(test.degenes.out[test.degenes.out$BH < 0.05 | is.na(test.degenes.out$BH),])[1])

        
        #write result tables for future analysis    
        write.table(test.degenes.out, file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_", ontology, "_", test.type, "_" ,suffix, "_", algorithm, ".out"), quote=F, row.names=F, sep="\t")
        
        #collating summary stats
        summary.stats <- as.data.frame(rbind(test.stats))
#        names(summary.stats) <- c("test", "tp", "ontology", "algorithm", "genes", "annot genes", "sig genes", "annot terms", "sig terms", "sig terms BH", "sig terms qvalue")
        names(summary.stats) <- c("test", "tp", "ontology", "algorithm", "genes", "annot genes", "sig genes", "annot terms", "sig terms", "sig terms BH")
        overall.stats <- rbind(overall.stats, summary.stats)
        
        #making plots!
        #printGraph(test.degenes, test.degenes.fisher, firstSigNodes=10, useInfo="all", fn.prefix=paste("decay_GO_", ontology, "_", test.type, "_" ,suffix, sep=""),_no_fail.pdfSW=T)
    }
    return(overall.stats)
}

#Prep the function:
overall.stats <- data.frame()
test.type <- "all"

overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05,]$genes, topGenesLoess$genes, "BP", "all_genes_no_fail_fdr_0.05", "classic")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05,]$genes, topGenesLoess$genes, "MF", "all_genes_no_fail_fdr_0.05", "classic")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05,]$genes, topGenesLoess$genes, "CC", "all_genes_no_fail_fdr_0.05", "classic")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01,]$genes, topGenesLoess$genes, "BP", "all_genes_no_fail_fdr_0.01", "classic")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01,]$genes, topGenesLoess$genes, "MF", "all_genes_no_fail_fdr_0.01", "classic")
overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01,]$genes, topGenesLoess$genes, "CC", "all_genes_no_fail_fdr_0.01", "classic")

# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "BP", "all_genes_no_fail_fdr_0.05_logfc", "classic")
# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "MF", "all_genes_no_fail_fdr_0.05_logfc", "classic")
# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "CC", "all_genes_no_fail_fdr_0.05_logfc", "classic")
# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "BP", "all_genes_no_fail_fdr_0.01_logfc", "classic")
# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "MF", "all_genes_no_fail_fdr_0.01_logfc", "classic")
# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "CC", "all_genes_no_fail_fdr_0.01_logfc", "classic")

# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05,]$genes, topGenesLoess$genes, "BP", "all_genes_no_fail_fdr_0.05", "elim")
# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05,]$genes, topGenesLoess$genes, "MF", "all_genes_no_fail_fdr_0.05", "elim")
# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05,]$genes, topGenesLoess$genes, "CC", "all_genes_no_fail_fdr_0.05", "elim")
# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01,]$genes, topGenesLoess$genes, "BP", "all_genes_no_fail_fdr_0.01", "elim")
# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01,]$genes, topGenesLoess$genes, "MF", "all_genes_no_fail_fdr_0.01", "elim")
# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01,]$genes, topGenesLoess$genes, "CC", "all_genes_no_fail_fdr_0.01", "elim")

# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "BP", "all_genes_no_fail_fdr_0.05_logfc", "elim")
# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "MF", "all_genes_no_fail_fdr_0.05_logfc", "elim")
# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "CC", "all_genes_no_fail_fdr_0.05_logfc", "elim")
# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "BP", "all_genes_no_fail_fdr_0.01_logfc", "elim")
# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "MF", "all_genes_no_fail_fdr_0.01_logfc", "elim")
# overall.stats <- de.testing(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01 & abs(topGenesLoess$logFC) >= 2,]$genes, topGenesLoess$genes, "CC", "all_genes_no_fail_fdr_0.01_logfc", "elim")

write.table(overall.stats, file=paste0(extraVars[1], "_", extraVars[2], "_GO_testing_summaries_no_fail.out"), col.names=T, row.names=F, quote=F, sep="\t", eol="\n")

#And now with the sex data: 
    overall.stats <- data.frame()
    test.type <- "all"

    overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.05,]$genes, topGenesLoessSex$genes, "BP", "all_genes_sex_no_fail_fdr_0.05", "classic")
    overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.05,]$genes, topGenesLoessSex$genes, "MF", "all_genes_sex_no_fail_fdr_0.05", "classic")
    overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.05,]$genes, topGenesLoessSex$genes, "CC", "all_genes_sex_no_fail_fdr_0.05", "classic")
    overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.01,]$genes, topGenesLoessSex$genes, "BP", "all_genes_sex_no_fail_fdr_0.01", "classic")
    overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.01,]$genes, topGenesLoessSex$genes, "MF", "all_genes_sex_no_fail_fdr_0.01", "classic")
    overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.01,]$genes, topGenesLoessSex$genes, "CC", "all_genes_sex_no_fail_fdr_0.01", "classic")

    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.05 & abs(topGenesLoessSex$logFC) >= 2,]$genes, topGenesLoessSex$genes, "BP", "all_genes_sex_no_fail_fdr_0.05_logfc", "classic")
    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.05 & abs(topGenesLoessSex$logFC) >= 2,]$genes, topGenesLoessSex$genes, "MF", "all_genes_sex_no_fail_fdr_0.05_logfc", "classic")
    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.05 & abs(topGenesLoessSex$logFC) >= 2,]$genes, topGenesLoessSex$genes, "CC", "all_genes_sex_no_fail_fdr_0.05_logfc", "classic")
    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.01 & abs(topGenesLoessSex$logFC) >= 2,]$genes, topGenesLoessSex$genes, "BP", "all_genes_sex_no_fail_fdr_0.01_logfc", "classic")
    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.01 & abs(topGenesLoessSex$logFC) >= 2,]$genes, topGenesLoessSex$genes, "MF", "all_genes_sex_no_fail_fdr_0.01_logfc", "classic")
    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.01 & abs(topGenesLoessSex$logFC) >= 2,]$genes, topGenesLoessSex$genes, "CC", "all_genes_sex_no_fail_fdr_0.01_logfc", "classic")

    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.05,]$genes, topGenesLoessSex$genes, "BP", "all_genes_sex_no_fail_fdr_0.05", "elim")
    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.05,]$genes, topGenesLoessSex$genes, "MF", "all_genes_sex_no_fail_fdr_0.05", "elim")
    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.05,]$genes, topGenesLoessSex$genes, "CC", "all_genes_sex_no_fail_fdr_0.05", "elim")
    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.01,]$genes, topGenesLoessSex$genes, "BP", "all_genes_sex_no_fail_fdr_0.01", "elim")
    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.01,]$genes, topGenesLoessSex$genes, "MF", "all_genes_sex_no_fail_fdr_0.01", "elim")
    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.01,]$genes, topGenesLoessSex$genes, "CC", "all_genes_sex_no_fail_fdr_0.01", "elim")

    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.05 & abs(topGenesLoessSex$logFC) >= 2,]$genes, topGenesLoessSex$genes, "BP", "all_genes_sex_no_fail_fdr_0.05_logfc", "elim")
    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.05 & abs(topGenesLoessSex$logFC) >= 2,]$genes, topGenesLoessSex$genes, "MF", "all_genes_sex_no_fail_fdr_0.05_logfc", "elim")
    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.05 & abs(topGenesLoessSex$logFC) >= 2,]$genes, topGenesLoessSex$genes, "CC", "all_genes_sex_no_fail_fdr_0.05_logfc", "elim")
    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.01 & abs(topGenesLoessSex$logFC) >= 2,]$genes, topGenesLoessSex$genes, "BP", "all_genes_sex_no_fail_fdr_0.01_logfc", "elim")
    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.01 & abs(topGenesLoessSex$logFC) >= 2,]$genes, topGenesLoessSex$genes, "MF", "all_genes_sex_no_fail_fdr_0.01_logfc", "elim")
    # overall.stats <- de.testing(topGenesLoessSex[topGenesLoessSex$adj.P.Val <= 0.01 & abs(topGenesLoessSex$logFC) >= 2,]$genes, topGenesLoessSex$genes, "CC", "all_genes_sex_no_fail_fdr_0.01_logfc", "elim")

    write.table(overall.stats, file=paste0(extraVars[1], "_", extraVars[2], "_GO_testing_summaries_sex_no_fail.out"), col.names=T, row.names=F, quote=F, sep="\t", eol="\n")


print("Testing for enrichments in Reilly et al and Zeng et al datasets. This takes a while because there's a lot of permutations .")

# And a comparison with Steven Reilly's data:
reilly <- read.table("../reilley_et_al_2015_enriched_genes.txt", header=F)
length(which(topGenesLoess$genes %in% reilly$V1))
# [1] 96
length(which(topGenesLoess[topGenesLoess$adj.P.Val <= 0.1,]$genes %in% reilly$V1))
# [1] 16
length(which(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05,]$genes %in% reilly$V1))
# [1] 8
length(which(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01,]$genes %in% reilly$V1))
# [1] 3

# Also with Zeng et al, 2012:
zeng <- read.table("../zeng_et_al_2012_DM_genes.txt", header=F)
# These need ENSEMBL identifiers, since they're using NCBI
zengEnsembl <- getBM(attributes=c("ensembl_gene_id", "refseq_mrna"), mart=ensembl)
zeng$ensembl <- zengEnsembl[match(zeng$V1, zengEnsembl$refseq_mrna),1]

length(which(topGenesLoess$genes %in% zeng$ensembl))
# [1] 355
length(which(topGenesLoess[topGenesLoess$adj.P.Val <= 0.1,]$genes %in% zeng$ensembl))
# [1] 58
length(which(topGenesLoess[topGenesLoess$adj.P.Val <= 0.05,]$genes %in% zeng$ensembl))
# [1] 37
length(which(topGenesLoess[topGenesLoess$adj.P.Val <= 0.01,]$genes %in% zeng$ensembl))
# [1] 19

# Write the output:
write.table(topGenesLoess[topGenesLoess$genes %in% reilly$V1,], file=paste0(extraVars[1], "_", extraVars[2], "_DE_testing_reilly_overlap_no_fail.out"), col.names=T, row.names=F, quote=F, sep="\t", eol="\n")
write.table(topGenesLoess[topGenesLoess$genes %in% zeng$ensembl,], file=paste0(extraVars[1], "_", extraVars[2], "_DE_testing_zeng_overlap_no_fail.out"), col.names=T, row.names=F, quote=F, sep="\t", eol="\n")

### This is not the best way of testing, since we have no idea of how many genes there were in the Reilly data to begin with, but:
loessSig01 <- topGenesLoess[topGenesLoess$adj.P.Val <= 0.01,]
loessSig05 <- topGenesLoess[topGenesLoess$adj.P.Val <= 0.05,] 
loessSig10 <- topGenesLoess[topGenesLoess$adj.P.Val <= 0.10,] 

permutation.stats <- data.frame()

for (i in 1:20000) {
  #sample DE genes from all the data sets, array sized: 
  genesDE01 <- sample(1:dim(topGenesLoess)[1], dim(loessSig01)[1], replace=F)
  genesDE05 <- sample(1:dim(topGenesLoess)[1], dim(loessSig05)[1], replace=F)
  genesDE10 <- sample(1:dim(topGenesLoess)[1], dim(loessSig10)[1], replace=F)
  reillySig <- sample(1:dim(topGenesLoess)[1], length(which(topGenesLoess$genes %in% reilly$V1)), replace=F)
  zengSig <- sample(1:dim(topGenesLoess)[1], length(which(topGenesLoess$genes %in% zeng$ensembl)), replace=F)

  #overlap expression and atac:
  reilly01 <- table(!is.na(match(reillySig, genesDE01)))[2]
  reilly05 <- table(!is.na(match(reillySig, genesDE05)))[2]
  reilly10 <- table(!is.na(match(reillySig, genesDE10)))[2]
  zeng01 <- table(!is.na(match(zengSig, genesDE01)))[2]
  zeng05 <- table(!is.na(match(zengSig, genesDE05)))[2]
  zeng10 <- table(!is.na(match(zengSig, genesDE10)))[2]

  permutation.stats <- rbind(permutation.stats, cbind(reilly01, reilly05, reilly10, zeng01, zeng05, zeng10))
}

write.table(permutation.stats, file="zeng_et_al_2012_reilly_et_al_2015_permutation_tests.txt", quote=F, col.names=T, row.names=F)

dim(permutation.stats[permutation.stats$reilly01 >= length(which(loessSig01$genes %in% reilly$V1)),])
dim(permutation.stats[permutation.stats$reilly05 >= length(which(loessSig05$genes %in% reilly$V1)),])
dim(permutation.stats[permutation.stats$reilly10 >= length(which(loessSig10$genes %in% reilly$V1)),])
dim(permutation.stats[permutation.stats$zeng01 >= length(which(loessSig01$genes %in% zeng$V1)),])
dim(permutation.stats[permutation.stats$zeng05 >= length(which(loessSig05$genes %in% zeng$V1)),])
dim(permutation.stats[permutation.stats$zeng10 >= length(which(loessSig10$genes %in% zeng$V1)),])


###################################################
### 6. GO TESTING WITH GOSEQ INSTEAD OF TOPGO. ###
###################################################

    # # First thing is generating a vector of 0s and 1s, where 1s correspond to DE and 0s to non DE. Gene names should be element names
    # deGenesLoess <- ifelse(topGenesLoess$adj.P.Val <= 0.05, 1, 0)
    # names(deGenesLoess) <- topGenesLoess$genes
    # deGenesLoess <- deGenesLoess[order(names(deGenesLoess))]

    # # Need to supply custom gene lengths, but the ontology mappings should be retained - there's no good reason to discard them. 
    # meanLength <- (mergedCounts$hsLength + mergedCounts$mfLength) /2
    # deGenesPWF <- nullp(deGenesLoess, bias.data=(meanLength[which(mergedCounts$Geneid %in% names(deGenesLoess))]))

    # # Orthology mapping - let's try it first using the GO terms for hg19 and seeing how those compare to manually fetching hg38, which will take far longer
    # deGoCats <- goseq(deGenesPWF, "hg19", "ensGene")
    # deGoCats$qvalue <- qvalue(deGoCats$over_represented_pvalue)$qvalues

    # # There's not much exciting there, and also I would wager not much disagreement with topGO. But let's try KEGG instead:
    # # Code taken from the goseq manual:
    # # Get the mapping from ENSEMBL 2 Entrez
    # en2eg <- as.list(org.Hs.egENSEMBL2EG)
    # # Get the mapping from Entrez 2 KEGG
    # eg2kegg<- as.list(org.Hs.egPATH)
    # # Define a function which gets all unique KEGG IDs
    # # associated with a set of Entrez IDs
    # grepKEGG <- function(id,mapkeys){unique(unlist(mapkeys[id],use.names=FALSE))} # Apply this function to every entry in the mapping from ENSEMBL 2 Entrez to combine the two maps
    # kegg <- lapply(en2eg,grepKEGG,eg2kegg)

    # deGenesKegg <- goseq(deGenesPWF, gene2cat=kegg)
    # deGenesKegg$qvalue <- qvalue(deGenesKegg$over_represented_pvalue)$qvalues

    # deGenesLoess <- ifelse(topGenesLoess$adj.P.Val <= 0.01, 1, 0)
    # names(deGenesLoess) <- topGenesLoess$genes
    # deGenesLoess <- deGenesLoess[order(names(deGenesLoess))]

    # # Need to supply custom gene lengths, but the ontology mappings should be retained - there's no good reason to discard them. 
    # meanLength <- (mergedCounts$hsLength + mergedCounts$mfLength) /2
    # deGenesPWF <- nullp(deGenesLoess, bias.data=(meanLength[which(mergedCounts$Geneid %in% names(deGenesLoess))]))

    # deGenesKegg <- goseq(deGenesPWF, gene2cat=kegg)
    # deGenesKegg$qvalue <- qvalue(deGenesKegg$over_represented_pvalue)$qvalues


#########################################
### 7. COMPARE SEX AND NO SEX MODELS. ###
#########################################

print ("Making Venn diagrams:")


make.venn.pair <- function(geneset1, geneset2, prefix, geneset1.label, geneset2.label,universe){
  pdf(file=paste(prefix, "_no_fail.pdf", sep=""), width=7, height=7)
  venn.placeholder <- draw.pairwise.venn(length(geneset1),length(geneset2), length(which(geneset1 %in% geneset2) == TRUE), c(paste(geneset1.label, length(geneset1), sep="\n"), paste(geneset2.label, length(geneset2), sep="\n")), fill=c("goldenrod", "plum4"), alpha=c(0.5, 0.5),col=NA, euler.d=T)
  complement.size <- dim(universe)[1] - length(geneset1) - length(geneset2) + length(which(geneset1 %in% geneset2) == TRUE)
  grid.text(paste(complement.size, " not DA'ed\nin either", sep=""), x=0.1, y=0.1)
  dev.off()
  print(paste("Genes in a: ", length(geneset1), sep=""))
  print(paste("Genes in b: ", length(geneset2), sep=""))
  print(paste("Common genes: ", length(which(geneset1 %in% geneset2) == TRUE), sep=""))
}

make.venn.pair(topGenesLoess[topGenesLoess$adj.P.Val < 0.01,]$genes, topGenesLoessSex[topGenesLoessSex$adj.P.Val < 0.01,]$genes, paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_sex_no_sex_FDR_1"), "species", "species + sex", topGenesLoess)

make.venn.pair(topGenesLoess[topGenesLoess$adj.P.Val < 0.05,]$genes, topGenesLoessSex[topGenesLoessSex$adj.P.Val < 0.05,]$genes, paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_sex_no_sex_FDR_5"), "species", "species + sex", topGenesLoess)

make.venn.pair(topGenesLoess[topGenesLoess$adj.P.Val < 0.10,]$genes, topGenesLoessSex[topGenesLoessSex$adj.P.Val < 0.10,]$genes, paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_sex_no_sex_FDR_10"), "species", "species + sex", topGenesLoess)

