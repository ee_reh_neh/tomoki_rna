#!/bin/bash

# Calculates GC content across all retained orthologous exons of genes from featureCounts-style SAF files.
# Requires two command line arguments - input directory and output directory.
#
# Usage: get_gc_content_by_gene.sh [[in dir]] [[out dir]]
#
# IGR, 16.08.17

safdir=$1
outdir=$2

for longName in `ls $safdir`; do
    sampName=`basename $longName .txt`
    echo "Calculating GC content for $sampName..."
    if echo $sampName | grep -q "^mac"; then
        genome="/home/users/ntu/igr/orthoexon/macFas5/macFas5.fa"
        sed '1d' $safdir/$longName | awk '{print $2 "\t" $3 "\t" $4 "\t" $5 "\t" $6  "\t" $1}' | bedtools nuc -fi $genome -bed stdin | awk '{print $5 "\t" $6 "\t" $10 "\t" $11 "\t" $15}' | sed '1d' |Rscript --vanilla ~/repos/tomoki_rna/get_gc_content_by_gene.r $outdir/${sampName}_gc.out
    elif echo $sampName | grep -q "^pan"; then
        genome="/home/users/ntu/igr/orthoexon/panTro3/panTro3_clean_names.fa"
        sed '1d' $safdir/$longName | awk '{print $2 "\t" $3 "\t" $4 "\t" $5 "\t" $6  "\t" $1}' | bedtools nuc -fi $genome -bed stdin | awk '{print $5 "\t" $6 "\t" $10 "\t" $11 "\t" $15}' | sed '1d' |Rscript --vanilla ~/repos/tomoki_rna/get_gc_content_by_gene.r $outdir/${sampName}_gc.out
    elif echo $sampName | grep -q "^hg"; then
        genome="/home/users/ntu/igr/orthoexon/hg38/hg38_renamed_no_alt_analysis_set.fa"
        sed '1d' $safdir/$longName | awk '{print $2 "\t" $3 "\t" $4 "\t" $5 "\t" $6  "\t" $1}' | bedtools nuc -fi $genome -bed stdin | awk '{print $5 "\t" $6 "\t" $10 "\t" $11 "\t" $15}' | sed '1d' |Rscript --vanilla ~/repos/tomoki_rna/get_gc_content_by_gene.r $outdir/${sampName}_gc.out
    fi    
done


