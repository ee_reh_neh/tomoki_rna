#!/bin/bash
# Created by the Melbourne Bioinformatics job script generator for SLURM
# Wed Jul 26 2017 13:19:32 GMT+1000 (AEST)

# Partition for the job:
#SBATCH -p sysgen

# The project ID which this job should run under:
#SBATCH --account="SG0008"

# Maximum number of tasks/CPU cores used by the job:
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10

# The amount of memory in megabytes per process in the job:
#SBATCH --mem-per-cpu=10240

# Send yourself an email when the job:
# aborts abnormally (fails)
#SBATCH --mail-type=FAIL
# ends successfully
#SBATCH --mail-type=END

# The maximum running time of the job in days-hours:mins:sec
#SBATCH --time=0-72:00:00

# The name of the job:
#SBATCH --job-name="macFas_ST"

# Export path variables.
#SBATCH --export=ALL

# Output control:
#SBATCH --workdir="/vlsci/SG0008/igallego/tomoki/superTranscripts/logs/"
#SBATCH --output="macFas_superTranscript.log"

# check that the script is launched with sbatch
if [ "x$SLURM_JOB_ID" == "x" ]; then
   echo "You need to submit your job to the queuing system with sbatch"
   exit 1
fi

# Output control pt 2:
# cd ~/tomoki/superTranscripts
cd ~/necklace_test

# The modules to load:
module load Java # java
module load SAMtools # samtools
module load Bowtie2 # bowtie2
module load HISAT2 # hisat2
module load StringTie # stringtie
module load BLAT # blat
module load TrinityRNASeq # Trinity

# The job command(s):

~/bin/necklace-necklace_v0.9/tools/bin/bpipe run ~/repos/tomoki_rna/necklace/necklace.groovy data/data.txt



