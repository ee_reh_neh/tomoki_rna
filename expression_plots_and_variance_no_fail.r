### expression_plots_and_variance.R
### For experimental analysis of RNA-seq data from human and crab-eating macaque neural progenitors. 
### Modelled closely on main_analysis_final.R from IGR's chimp-human iPSC paper
### IGR 16.08.14

### Split the original expression_eda.R file into two, with this one not focusing on DE testing or any of that downstream stuff, which gets shunted to expression_de_testing.R instead. 
### IGR 16.12.08

### Changed paths to Ensembl 86
### IGR 17.07.26

### 0. PREPARE WORKSPACE. ###
### 1. READ IN COUNT MATRICES AND EXON LISTS. ### 
### 2. CALCULATE NORMALISED LIBRARY SIZES, GENERATE BASIC DESCRIPTIVE PLOTS. ###
### 3. COMPUTE CPM, FILTER, LOESS NORMALISE, AND COMPUTE RPKM. ###
### 4. LOOK AT REPRODUCIBILITY BETWEEN DIFFERENT LEVELS OF REPLICATES. ###
### 5. PCA, TESTING POSSIBLE COVARIATES AND DRIVERS, AND OTHER BASIC VISUAL DESCRIPTIONS. ###
### 6. NEIGHBOR-JOINING TREES AS IN THE IPSC PAPER. ###
### 7. EXAMINE SOURCES OF VARIATION, LOOK AT GENES SEPARATELY. ###
### 8. INTRA-CLASS CORRELATION-ISH ANALYSES. ###

###     echo -e "#! /bin/bash; module load R; Rscript --vanilla ~/repos/tomoki_rna/expression_plots_and_variance.r" | sbatch --time=4:00:00 --mem=4096 --cpus-per-task=1 -p sysgen --account=SG0008 --mail-type=FAIL,END -_no_fail.output ~/tomoki/rpkm/ensembl86/logs/expression_plots_and_variance.log --job-name=plots_var --export=ALL

#############################
### 0. PREPARE WORKSPACE. ###
#############################

#extraVars <- commandArgs(trailingOnly=T)
extraVars <- c("ortho_FALSE", 0.92) 

### load libraries
library("gplots")
library(RColorBrewer)
library(limma)
library(edgeR)
library(statmod)
library(beeswarm)
library(biomaRt)
library(topGO)
library(SparseM)
library(org.Hs.eg.db)
# library(goseq)
library(qvalue)
library(variancePartition)
library(ape)
library(ggplot2)
library(reshape2)
library(plyr)

options(width=200)
# setwd("~/tomoki/rpkm/ensembl86")
setwd("~/cloudstor/Data/tomoki/second_batch/")
pal <- c(brewer.pal(9, "BuPu"), "black", brewer.pal(6, "YlOrRd"))

#################################################
### 1. READ IN COUNT MATRICES AND EXON LISTS. ### 
#################################################

hsOrthoCounts <- read.table(paste0("human_", extraVars[2], "pc_th_first_fc_2_", extraVars[1], "_clean.out"), header=T)
mfOrthoCounts <- read.table(paste0("macFas_", extraVars[2], "pc_th_first_fc_2_", extraVars[1], "_clean.out"), header=T)

mergedCounts <- merge(hsOrthoCounts[,c(1,7:17)], mfOrthoCounts[,c(1,7:15)], by.x="Geneid", by.y="Geneid", all=T)
names(mergedCounts) <- gsub("\\.fastq","", names(mergedCounts))
dim(mergedCounts)
# [1] 34142    21

# Remove unwanted individiuals - the failed individual with very very few reads overall: 
mergedCounts <- mergedCounts[,colnames(mergedCounts) != "EDI2.8_S5"]
mergedCounts <- mergedCounts[,!grepl("MFTW1|H9.2|H9.5", colnames(mergedCounts))]

# Fix the EDI names and convert to EDI2:
names(mergedCounts) <- gsub("EDI\\.", "EDI2\\.", names(mergedCounts))

### OPTIONAL THINGS TO DO:
    # Filter all ribosomal protein genes because they are terrible:
    ensembl <- useMart("ENSEMBL_MART_ENSEMBL", dataset="hsapiens_gene_ensembl", host="oct2016.archive.ensembl.org") # Ensembl 86, for continuity.
    geneFamilies <- getBM(attributes=c("ensembl_gene_id", "family", "family_description", "external_gene_name"), mart=ensembl)
    badRibosomes <- geneFamilies[grepl("MRP|RPS|RPL|RP L|RP S", geneFamilies$family_description),1] # Filtering genes associated with nasty ribosomal families - I wonder how effective this will be downstream!
    moreBadRibos <- geneFamilies[grepl("MRP|RPS|RPL|RP L|RP S", geneFamilies$external_gene_name),1]

    allBadRibos <- c(badRibosomes, moreBadRibos, "ENSG00000104517") # This final gene used to get filtered_no_fail.out, and as of 2016.12.20 no longer does - I don't understand why either before or after, but to keep it consistent I will simply exclude it manually. 

    mergedCounts <- mergedCounts[!mergedCounts$Geneid %in% allBadRibos,]
    dim(mergedCounts)
    # [1]  33669    15

    # For RPKM calculation later
    mergedCounts$hsLength <- hsOrthoCounts[!hsOrthoCounts$Geneid %in% allBadRibos,6]
    mergedCounts$mfLength <- mfOrthoCounts[!mfOrthoCounts$Geneid %in% allBadRibos,6]

    # drop non-autosomal genes
    # ychr.drop <- c("ENSG00000183878", "ENSG00000198692", "ENSG00000234652", "ENSG00000243576", "ENSG00000252472")
    # gene.lengths <- gene.lengths[!gene.lengths$GeneEnsemblID %in% ychr.drop,]

#prepare meta information from sample names:
countsNames <- colnames(mergedCounts[,2:15])
speciesCol <- c(rep("darkorchid4", 8), rep("orange", 6))
samplesMeta <- data.frame(countsNames, speciesCol)
names(samplesMeta) <- c("line", "col")
samplesMeta$cex <- 1.5

    #alternative colours etc:
    samplesMeta$species <- ifelse(grepl("^M", samplesMeta$line), "Mf", "Hs")
    samplesMeta$ind <- ifelse(grepl("^MF12", samplesMeta$line), "Mf12", ifelse(grepl("^MFTW", samplesMeta$line), "MfTW1", ifelse(grepl("^MF1", samplesMeta$line), "MF1", ifelse(grepl("^EDI2", samplesMeta$line), "EDI2", ifelse(grepl("^EDI", samplesMeta$line), "EDI", "H9")))))
    samplesMeta$sex <- ifelse(grepl("^MF12", samplesMeta$line), "M", "F")
    samplesMeta$indPch <- 14 + as.numeric(as.factor(samplesMeta$ind))

    indPal <- brewer.pal(6, "Dark2")
    samplesMeta$indPal <- indPal[as.numeric(as.factor(samplesMeta$ind))]

#Finally, add info on the covariates
covariates <- read.table("../sample_covariates.txt", header=T)
covariates$sample <- gsub("\\.fastq\\.gz","", covariates$sample)
covariates$sample <- gsub("-","\\.", covariates$sample)
names(covariates)[1] <- "line" # Need to rename for join to work in the next line. 
samplesMeta <- plyr::join(samplesMeta, covariates) 

# save(samplesMeta, file="samples_meta_expression_final.Rda")

################################################################################
### 2. CALCULATE NORMALISED LIBRARY SIZES, GENERATE BASIC DESCRIPTIVE PLOTS. ###
################################################################################

# reads into edgeR, calculate TMM and then CPM, writing_no_fail.out the intermediate steps:
mergedCountsDge <- DGEList(counts=as.matrix(mergedCounts[,2:15]), genes=mergedCounts[,1])
mergedCountsDge <- calcNormFactors(mergedCountsDge)
#save(mergedCountsDge, file="ipsc_genes_dge_ipsc_final.Rda")
#write.table(mergedCountsDge$samples, file="TMM.normFactors_ipsc_genes_ipsc_final.txt", sep="\t", quote=F, row.names=T)

# some barplots a_no_fail.out mapping stats
# "Raw" library sizes:
pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_mapped_reads_raw_ipsc_clean_no_fail.pdf"))
mp <- barplot(sort(colSums(mergedCountsDge$counts)), ylab="Number of reads mapped to orthologous exons", xlab="", col="darkgrey", xaxt="n")
text(mp, -200000, srt = 45, adj = 1, labels = names(sort(colSums(mergedCountsDge$counts))), xpd = TRUE, cex=0.8)
dev.off()

# normalised library sizes
pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_mapped_reads_normalised_ipsc_clean_no_fail.pdf"))
mp <- barplot(sort(mergedCountsDge$samples$lib.size * mergedCountsDge$samples$norm.factor), ylab="Normalized library sizes", xlab="", xaxt="n", col="darkgrey")
text(mp, -200000, srt = 45, adj = 1, labels = row.names(mergedCountsDge$samples[order(mergedCountsDge$samples$lib.size * mergedCountsDge$samples$norm.factor), ]), xpd = TRUE, cex=0.8)
dev.off()

# number of genes expressed
pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_number_of_genes_expressed_ipsc_clean_no_fail.pdf"))
some.counts <- apply(mergedCountsDge$counts, 2, function(x) { sum(x > 0) })
mp <- barplot(sort(some.counts), ylab="Number of genes with at least 1 read", xlab="", xaxt="n", col="darkgrey")
text(mp, -500, srt = 45, adj = 1, labels = names(sort(some.counts)), xpd = TRUE, cex=0.8)
dev.off()

# Perform rarefaction curves for number of expressed genes vs. proportion of pool mRNA
# As in Ramskold D, Wang ET, Burge CB, Sandberg R. 2009. An abundance of ubiquitously expressed genes revealed by tissue transcriptome sequence data. PLoS Comput Biol 5:e1000598.
# This gives an idea of the complexity of transcriptome in different tissues

# using ortho Exons genes aggregates
pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_rarefaction_curves_ipsc_clean_no_fail.pdf"))
plot(1:length(mergedCountsDge$counts[,1]), cumsum(sort(mergedCountsDge$counts[,1], decreasing=T)/sum(mergedCountsDge$counts[,1])), log="x", type="n", xlab="Number of genes", ylab="Fraction of reads pool", ylim=c(0,1)) ## initialize the plot area
for (sample in colnames(mergedCountsDge)){
  lines(1:length(mergedCountsDge$counts[,sample]), cumsum(sort(mergedCountsDge$counts[,sample], decreasing=T)/sum(mergedCountsDge$counts[,sample])), col=pal[which(grepl(sample, colnames(mergedCountsDge)))], lwd=2)
#  lines(1:length(mergedCountsDge$counts[,sample]), cumsum(sort(mergedCountsDge$counts[,sample], decreasing=T)/sum(mergedCountsDge$counts[,sample])), col=as.character(samplesMeta[samplesMeta$line %in% sample,]$col), lwd=2)
}
legend(x="topleft", bty="n", col=c(pal), legend=samplesMeta$line, lty=1, lwd=2)
dev.off()


##################################################################
### 3. COMPUTE CPM, FILTER, LOESS NORMALISE, AND COMPUTE RPKM. ###
##################################################################

### Calculate log2 CPM
cpmNorm <- cpm(mergedCountsDge, normalized.lib.sizes=TRUE, log=TRUE, prior.count=0.25) ### I may come back and try this with the Irizarry prior of 0.5, because it makes more intuitive sense to me... but this one is simply -2, so really, no difference
pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_cpm.density_ipsc_clean_no_fail.pdf"))
plotDensities(cpmNorm, group=samplesMeta$species) 
abline(v=1)
dev.off()

# Filter on observing cpm greater or equal to 1 or more in at least half of the individuals in one species, not keeping library sizes.
mergedCountsDgeFilt <- mergedCountsDge[rowSums(cpmNorm[,1:8] >= 2) >= 4 | rowSums(cpmNorm[,9:14] >= 2) >= 3 , , keep.lib.sizes=F] 
dim(mergedCountsDgeFilt)
#[1] 11051    14

pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_cpmFilt.density_ipsc_clean_no_fail.pdf"))
plotDensities(mergedCountsDgeFilt, group=samplesMeta$species)
dev.off()

# Recalculate TMM and lib sizes
mergedCountsDgeFilt <- calcNormFactors(mergedCountsDgeFilt) ## recalculate norm factors
# save(mergedCountsDgeFilt, file="mergedCountsDge.TMMFilt_clean.RDa")

# Loess normalise the data
## For starters, Voom requires a design matrix as input
design <- model.matrix(~ 0 + samplesMeta$species + as.factor(samplesMeta$sex) + as.factor(samplesMeta$seqBatch))
colnames(design) <- c("human", "macFas", "sex", "seqBatch2", "seqBatch3")

## Voom on filtered nonGC normalized data, with cyclic loess normalization, and blocked by individual replicates - this requires two passes, one wit_no_fail.out the random individual effect and a second one that takes it into account - see the limma manual and this post and reply by Gordon Smyth!
## https://support.bioconductor.org/p/59700/

## note that cyclic loess is designed for between-array normalisation rather than RNA-seq, but it should still be usable. Ideally we would not need it, however, but I don't like the directionality of the results wit_no_fail.out normalisation, not to mention there's a clear_no_fail.outlier sample in there. 
cpmNormLoess <- voom(mergedCountsDgeFilt, design, normalize.method="cyclicloess", plot=F) 
cpmCorfit <- duplicateCorrelation(cpmNormLoess, design, block=samplesMeta$ind)
    #Gives six warnings with final data set
    cpmCorfit$consensus
# [1] 0.5169432
pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_voom.loess.cpm.indrandom_no_fail.output_clean_no_fail.pdf"))
cpmNormLoess <- voom(mergedCountsDgeFilt, design, normalize.method="cyclicloess", plot=T, correlation=cpmCorfit$consensus, block=samplesMeta$ind) 
dev.off()

# Second round of duplicate correlations, as recommended by Gordon Smyth. 
cpmCorfit <- duplicateCorrelation(cpmNormLoess, design, block=samplesMeta$ind)
    #Gives eight warnings with final data set. 
cpmCorfit$consensus
# [1] 0.5276686

pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_voom.loess.cpm.indrandom.density_clean_no_fail.pdf"))
plotDensities(cpmNormLoess, group=samplesMeta$species) 
dev.off()

## Turn this into RPKM
# Values are already log2 CPM, so just need to substract
rpkmNormLoess <- cpmNormLoess
rpkmNormLoess$E[,1:8] <- cpmNormLoess$E[,1:8] - log2((mergedCounts[rowSums(cpmNorm[,1:8] > 2) >= 4 | rowSums(cpmNorm[,9:14] > 2) >= 3 , 16])/1000)
rpkmNormLoess$E[,9:14] <- cpmNormLoess$E[,9:14] - log2((mergedCounts[rowSums(cpmNorm[,1:8] >= 2) >= 4 | rowSums(cpmNorm[,9:14] >= 2) >= 3 , 17])/1000)

rpkmsForWrite <- data.frame(rpkmNormLoess$genes, rpkmNormLoess$E)
write.table(rpkmsForWrite, file=paste0(extraVars[1], "_", extraVars[2], "_voom.loess.rpkm.indrandom_no_fail.out"), col.names=T, row.names=F, quote=F, sep="\t", eol="\n")

pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_voom.loess.rpkm.indrandom.density_clean_no_fail.pdf"))
plotDensities(rpkmNormLoess, group=samplesMeta$species) #again, a beautiful distribution, as expected. 
dev.off()

##########################################################################
### 4. LOOK AT REPRODUCIBILITY BETWEEN DIFFERENT LEVELS OF REPLICATES. ###
##########################################################################

plot.reproducibility <- function(data.to.test, metadata, method){
    cor.mat <- cor(data.to.test, method=method, use="pairwise.complete.obs")

    ind.rep <- vector()
    ind.rep.col <- vector()
    batch.rep <- vector()
    batch.rep.col <- vector()
    species.rep <- vector()
    species.batch.rep <- vector()
    between.species <- vector()

    for (i in 1:ncol(data.to.test)){
        for (j in 1:ncol(data.to.test)){
            if (j > i){
                if (metadata$ind[i] == metadata$ind[j]) {
                    if(metadata$seqBatch[i] == metadata$seqBatch[j]){
                        ind.rep <- c(ind.rep, cor.mat[i,j])
                        ind.rep.col <- c(ind.rep.col, metadata$indPal[i])
                    } else{
                        batch.rep <- c(batch.rep, cor.mat[i,j])
                        batch.rep.col <- c(batch.rep.col, metadata$indPal[i])
                    }
                } else if (metadata$species[i] == metadata$species[j]){
                    if(metadata$seqBatch[i] == metadata$seqBatch[j]){
                        species.rep <- c(species.rep, cor.mat[i,j])
                    } else{
                        species.batch.rep <- c(species.batch.rep, cor.mat[i,j])
                    } 
                } else {between.species <- c(between.species, cor.mat[i,j])}
            }
        }
    }

    for.plot <- list(ind.rep, batch.rep, species.rep, species.batch.rep, between.species)
    boxplot(for.plot, ylab=paste0(method, " correlation"), names=c("Within\nindividuals", "Within ind,\nb/w seq batch", "Within\nspecies", "W/in species,\nb/w seq batch", "Between\nspecies"))
    beeswarm(for.plot[1], vertical = TRUE, method = "swarm", add = TRUE, pch = 20, cex=2, pwcol=ind.rep.col)
    beeswarm(for.plot[2], vertical = TRUE, method = "swarm", add = TRUE, pch = 20, cex=2, at=2, pwcol=batch.rep.col)
    stripchart(for.plot[3:5], vertical = TRUE, method = "jitter", add = TRUE, pch = 20, cex=2, at=3:5, col="grey50")
    legend(x="topright", legend=unique(metadata$ind), col=unique(metadata$indPal), bty="n", pch=20, cex=1.3)
}

pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_reproducibility_by_levels_clean_no_fail.pdf"))
plot.reproducibility(rpkmNormLoess$E, samplesMeta, "spearman")
plot.reproducibility(rpkmNormLoess$E, samplesMeta, "pearson")
dev.off()

#############################################################################################
### 6. PCA, TESTING POSSIBLE COVARIATES AND DRIVERS, AND OTHER BASIC VISUAL DESCRIPTIONS. ###
#############################################################################################

# Here is the PCA plotting function:
plot.pca <- function(dataToPca, speciesCol, namesPch, sampleNames){
    # check for invariant rows:
    dataToPca.clean <- dataToPca[!apply(dataToPca, 1, var) == 0,]
    pca <- prcomp(t(dataToPca.clean), scale=T, center=T)
    pca.var <- pca$sdev^2/sum(pca$sdev^2)
    plot(pca$x[,1], pca$x[,2], col=speciesCol, pch=namesPch, cex=2, xlab=paste("PC1 (", round(pca.var[1]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC2 (", round(pca.var[2]*100, digits=2), "% of variance)", sep=""))
    legend(col=speciesCol, legend=sampleNames, pch=namesPch, x="bottomright", cex=0.6)
    plot(pca$x[,2], pca$x[,3], col=speciesCol, pch=namesPch, cex=2, xlab=paste("PC2 (", round(pca.var[2]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC3 (", round(pca.var[3]*100, digits=2), "% of variance)", sep=""))
    legend(col=speciesCol, legend=sampleNames, pch=namesPch, x="topright", cex=0.6)
    plot(pca$x[,3], pca$x[,4], col=speciesCol, pch=namesPch, cex=2, xlab=paste("PC3 (", round(pca.var[3]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC4 (", round(pca.var[4]*100, digits=2), "% of variance)", sep=""))
    legend(col=speciesCol, legend=sampleNames, pch=namesPch, x="topright", cex=0.6)

    return(pca)

}

# And here is the PCA association function
pc.assoc <- function(pca.data){
    all.pcs <- data.frame()
    for (i in 1:ncol(pca.data$x)){
        all.assoc <- vector()
        for (j in 1:ncol(all.covars.df)){
            test.assoc <- anova(lm(pca.data$x[,i] ~ all.covars.df[,j]))[1,5]
            all.assoc <- c(all.assoc, test.assoc)
        }
        single.pc <- c(i, all.assoc)
        all.pcs <- rbind(all.pcs, single.pc)
    }
    names(all.pcs) <- c("PC", colnames(all.covars.df))

    print ("Here are the relationships between PCs and some possible covariates")
    print (all.pcs)
    return (all.pcs)
}

#Prepare covariate matrix, turn the right things into factors:
all.covars.df <- samplesMeta[,c(1,4,5,6,9:13,14)]
all.covars.df$species <- factor(all.covars.df$species)
all.covars.df$ind <- factor(all.covars.df$ind)
all.covars.df$sex <- factor(all.covars.df$sex)
all.covars.df$seqBatch <- factor(all.covars.df$seqBatch)
all.covars.df$batch <- factor(all.covars.df$batch)

# Actually plotting the PCA
pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_voom.loess.cpm.indrandom.pca_clean_no_fail.pdf"))
pcaresults <- plot.pca(rpkmNormLoess$E, pal, samplesMeta$indPch, samplesMeta$line)
dev.off()

all.pcs <- pc.assoc(pcaresults)

# Write_no_fail.out the covariates:
write.table(all.pcs, file=paste0(extraVars[1], "_", extraVars[2], "_pca_covariates_no_fail.out"), col.names=T, row.names=F, quote=F, sep="\t", eol="\n")

# Getting the loadings and top genes:
geneLoadings <- as.data.frame(pcaresults$rotation)
geneLoadings$genes <- rpkmNormLoess$genes
write.table(geneLoadings[order(-abs(geneLoadings$PC1)),][1:100,20], file=paste0(extraVars[1], "_", extraVars[2], "_pc1_top_100_absolute_loadings_no_fail.out"), quote=F, eol="\n", row.names=F, col.names=F)
write.table(geneLoadings[order(-abs(geneLoadings$PC2)),][1:100,20], file=paste0(extraVars[1], "_", extraVars[2], "_pc2_top_100_absolute_loadings_no_fail.out"), quote=F, eol="\n", row.names=F, col.names=F)

# RPKM correlation matrices
pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_voom.loess.cpm.indrandom.clustering_clean_no_fail.pdf"))
par(cex.main=0.8)
heatmap.2(cor(rpkmNormLoess$E, method="pearson", use="pairwise.complete.obs"), trace="none", main="Pearson correlation, RPKM", margins=c(8,8), srtCol=45, srtRow=45)
heatmap.2(cor(rpkmNormLoess$E, method="spearman", use="pairwise.complete.obs"), trace="none", main="Spearman correlation, RPKM", margins=c(8,8), srtCol=45, srtRow=45)
dev.off()


########################################################
### 6. NEIGHBOR-JOINING TREES AS IN THE IPSC PAPER. ###
########################################################

rpkm.dist.man <- dist(t(rpkmNormLoess$E), method="manhattan")
rpkm.dist.eu <- dist(t(rpkmNormLoess$E))

rpkm.nj.man <- nj(rpkm.dist.man)
rpkm.nj.eu <- nj(rpkm.dist.eu)

pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_voom.nj_trees_no_fail.pdf"))
plot(rpkm.nj.man, type="unrooted", cex=0.8, tip.color=samplesMeta$indPal, rotate=45)
plot(rpkm.nj.eu, type="unrooted", cex=0.8, tip.color=samplesMeta$indPal, rotate=45)
dev.off()


###################################################################
### 7. EXAMINE SOURCES OF VARIATION, LOOK AT GENES SEPARATELY. ###
###################################################################

# For now this is copied directly from the variancePartition manual:
# Set up multithreading:
library(doParallel) 
cl <- makeCluster(8) # Or the number of cores you have requested
registerDoParallel(cl)
samplesMeta$seqBatch <- as.factor(samplesMeta$seqBatch)
samplesMeta$batch <- as.factor(samplesMeta$batch)

# It seems all categorical variables are modelled as random effects here; it throws a warning if I try it any other way and refuses to work... so 
varParModelAllRandom <- ~ (1|species) + (1|sex) + (1|ind) + (1|seqBatch) + (1|batch)
varParModelNoSexRandom <- ~ (1|species) + (1|ind) + (1|seqBatch) + (1|batch)

varPartFitting <- fitExtractVarPartModel( rpkmNormLoess, varParModelAllRandom, samplesMeta )
varPartNoSexFitting <- fitExtractVarPartModel( rpkmNormLoess, varParModelNoSexRandom, samplesMeta )

# sort variables (i.e. columns) by median fraction of variance explained
vp <- sortCols( varPartFitting )
vpns <- sortCols( varPartNoSexFitting )

# Some basic plots:
pdf(file="eda_plots/variance_partition_top100_genes_varPart_no_fail.pdf")
plotPercentBars( vp[1:100,] )
plotPercentBars( vpns[1:100,] )
dev.off()

pdf(file="eda_plots/variance_partition_violinplots_varPart_no_fail.pdf")
plotVarPart( vp )
plotVarPart( vpns )
dev.off()

# These are interesting results! They suggest just as much of the variation is explained by the individuals as it is by the species, which is worrisome... so now let's break this down a bit more, and look more closely at the data. But it's also suggesting that these types of data are going to be inherently noisy. :(

# Or is it driven by some individual replicates and not all? Let's look in more detail
# Again, back to variancePartition manual:

# specify formula to model within/between individual variance separately for each tissue
# Note that including +0 ensures each tissue is modeled explicitly # Otherwise, the first tissue would be used as baseline
varPartModelSpecies <- ~ (species+0|ind) + (1|sex)
varPartModelSpeciesComplex <- ~ (species+0|ind) + (1|species) + (1|sex)

# fit model and extract variance percents
varPartFittingSpecies <- fitExtractVarPartModel( rpkmNormLoess, varPartModelSpecies, samplesMeta )
varPartFittingSpeciesComplex <- fitExtractVarPartModel( rpkmNormLoess, varPartModelSpeciesComplex, samplesMeta )

vpSpecies <- sortCols( varPartFittingSpecies )
vpSpeciesComp <- sortCols( varPartFittingSpeciesComplex )

varPartModelSpeciesNoSex <- ~ (species+0|ind) 
# fit model and extract variance percents
varPartFittingSpeciesNoSex <- fitExtractVarPartModel( rpkmNormLoess, varPartModelSpeciesNoSex, samplesMeta )
vpSpeciesNoSex <- sortCols( varPartFittingSpeciesNoSex )

# Some basic plots:
pdf(file="eda_plots/variance_partition_top100_genes_varPartSpecies_no_fail.pdf")
plotPercentBars( vpSpecies[1:100,] )
plotPercentBars( vpSpeciesNoSex[1:100,] )
dev.off()

pdf(file="eda_plots/variance_partition_violinplots_varPartSpecies_no_fail.pdf")
plotVarPart( vpSpecies )
plotVarPart( vpSpeciesNoSex )
dev.off()

# Finally, same deal, but by individuals now:
varPartModelInds <- ~ (ind+0|species) + (1|sex)

# fit model and extract variance percents
varPartFittingInds <- fitExtractVarPartModel( rpkmNormLoess, varPartModelInds, samplesMeta )
vpInds <- sortCols( varPartFittingInds )

# Some basic plots:
pdf(file="eda_plots/variance_partition_top100_genes_varPartInds_no_fail.pdf")
plotPercentBars( vpInds[1:100,] )
dev.off()

pdf(file="eda_plots/variance_partition_violinplots_varPartInds_no_fail.pdf")
plotVarPart( vpInds )
dev.off()

# It seems all categorical variables are modelled as random effects here; it throws a warning if I try it any other way and refuses to work... so 
varParModelBatch <- ~ (1|species) + (1|sex) + (1|ind) + (1|batch)
varParModelBatchNoSex <- ~ (1|species) + (1|ind) + (1|batch)

varParModelSeqBatch <- ~ (1|species) + (1|sex) + (1|ind) + (1|seqBatch)
varParModelSeqBatchNoSex <- ~ (1|species) + (1|ind) + (1|seqBatch)

varPartBatchFitting <- fitExtractVarPartModel( rpkmNormLoess, varParModelBatch, samplesMeta )
varPartBatchNoSexFitting <- fitExtractVarPartModel( rpkmNormLoess, varParModelBatchNoSex, samplesMeta )

varPartSeqBatchFitting <- fitExtractVarPartModel( rpkmNormLoess, varParModelSeqBatch, samplesMeta )
varPartSeqBatchNoSexFitting <- fitExtractVarPartModel( rpkmNormLoess, varParModelSeqBatchNoSex, samplesMeta )

# sort variables (i.e. columns) by median fraction of variance explained
vpb <- sortCols( varPartBatchFitting )
vpbns <- sortCols( varPartBatchNoSexFitting )
vpsb <- sortCols( varPartSeqBatchFitting )
vpsbns <- sortCols( varPartSeqBatchNoSexFitting )

# Some basic plots:

pdf(file="eda_plots/variance_partition_violinplots_varPart_batch_no_fail.pdf")
plotVarPart( vpb )
plotVarPart( vpbns )
plotVarPart( vpsb )
plotVarPart( vpsbns )
dev.off()

# Write_no_fail.out the variance data. The structure is a bit messy so we need to pull things_no_fail.out
vpsbDf <- as.data.frame(matrix(unlist(vpsb@.Data),ncol=6))
names(vpsbDf) <- c("ind", "seqBatch", "species", "sex", "Residuals", "GeneID")
vpsbDf$GeneID <- rpkmNormLoess$genes  
write.table(vpsbDf, file=paste0(extraVars[1], "_", extraVars[2], "_voom.loess.rpkm.indrandom.variancePartition_no_fail.out"), col.names=T, row.names=F, quote=F, sep="\t", eol="\n")


#################################################
### 8. INTRA-CLASS CORRELATION-ISH ANALYSES. ###
#################################################

# From the plots above, the humans are clearly far more variable than the macaques - and you can tell that from the data too. But what are these genes? 

#There are three approaches: Look at the variance partition_no_fail.output directly to identify genes with high variation between individuals, or compute the correlation in expression levels within a single individual. The latter can only be done for H9, since E1 whatever has only two replicates which is not enough to calculate a variance. Finally, we can also look at the residuals of the simple species + ind model, but it's unclear what else will be driving that signal, so better not to. 

# Suggested by John Blischak on email after I got in touch: 
# Calculate mean and sd within each individual
mean_within <- matrix(nrow = nrow(rpkmNormLoess),
                    ncol = length(unique(samplesMeta$ind)),
                    dimnames = list(rpkmNormLoess$genes$genes,
                                    sort(unique(samplesMeta$ind))))
sd_within <- matrix(nrow = nrow(rpkmNormLoess),
                    ncol = length(unique(samplesMeta$ind)),
                    dimnames = list(rpkmNormLoess$genes$genes,
                                    sort(unique(samplesMeta$ind))))
for (i in 1:nrow(rpkmNormLoess)) {
  mean_within[i, ] <- tapply(rpkmNormLoess$E[i, ], samplesMeta$ind, mean)
  sd_within[i, ] <- tapply(rpkmNormLoess$E[i, ], samplesMeta$ind, sd)
}

# Calculate mean and sd across individuals
mean_across <- apply(mean_within, 1, mean)
sd_across <- apply(mean_within, 1, sd)

# Calculate mean of sd within individuals
sd_within_mean <- apply(sd_within, 1, mean)

# Plot sd within (mean) versus across
pdf(file="eda_plots/mean_within_vs_sd_within_no_fail.pdf")
plot(sd_within_mean, sd_across,
     xlab = "Mean variance within individuals",
     ylab = "Variance across individuals", pch=16, col=rgb(0,0,0,alpha=0.3), cex=0.8)
abline(b=1, a=0, col="red", lty=2, lwd=2)
smoothScatter(sd_within_mean, sd_across,
     xlab = "Mean variance within individuals",
     ylab = "Variance across individuals", pch=16, col=rgb(0,0,0,alpha=0.3), cex=0.8)
abline(b=1, a=0, col="red", lty=2, lwd=2)
dev.off()

# Writable data set:
allVariance <- data.frame(mean_within,sd_within,mean_across,sd_across,sd_within_mean)
names(allVariance)[1:5] <- paste0("mean_within.", names(allVariance)[1:5])
names(allVariance)[6:10] <- paste0("sd_within.", names(allVariance)[6:10])
names(allVariance) <- gsub("\\.1", "", names(allVariance))
write.table(allVariance, file=paste0(extraVars[1], "_", extraVars[2], "_voom.loess.rpkm.indrandom.sd_within_across_no_fail.out"), col.names=T, row.names=F, quote=F, sep="\t", eol="\n")


# Genes that are just noisy, i.e. variable within and across individuals
intersect(names(sd_within_mean)[sd_within_mean > 2],
          names(sd_across)[sd_across > 2])

# Genes that are just noisy within individuals, but consistent across individuals
intersect(names(sd_within_mean)[sd_within_mean > 2], names(sd_across)[sd_across < 2])
    #Seems to me these would be a real pain to call, though!

# Genes that are hyper consistent within individuals, but variable across individuals
intrigue <- intersect(names(sd_within_mean)[sd_within_mean < 0.5], names(sd_across)[sd_across > 2])
length(intrigue)
# [1] 104

intrigueRPKM <- data.frame(rpkmNormLoess$E[rpkmNormLoess$genes$genes %in% intrigue,])
intrigueRPKM$ensembl <- rpkmNormLoess$genes$genes[rpkmNormLoess$genes$genes %in% intrigue]

meltedInt <- melt(intrigueRPKM)
meltedInt$ind <- as.factor(sapply(strsplit(as.character(meltedInt[,2]), "\\."), "[[", 1))

pdf(file="eda_plots/intriguing_genes_no_fail.pdf")    
        for (i in 1:length(intrigue)){
        print(ggplot(meltedInt[meltedInt$ensembl %in% intrigue[i],],  aes(x=ind, y=value, fill=ind)) + geom_dotplot(binaxis='y', stackdir='center') + theme_bw() + labs(title=intrigue[i], y="RPKM", x="") + theme(legend.title=element_blank()))
    }
dev.off()


intrigueDist <- dist(t(intrigueRPKM[,1:14]))
intrigueNJ <- nj(intrigueDist)

pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_voom.vary_across_not_within_nj_tree_no_fail.pdf"))
plot(intrigueNJ, type="unrooted", cex=0.8, tip.color=samplesMeta$indPal, rotate=45)
dev.off()

hyperVar <- sd_across[order(-sd_across)][1:100]
hyperVarRPKM <- data.frame(rpkmNormLoess$E[rpkmNormLoess$genes$genes %in% names(hyperVar),])
hyperVarRPKM$ensembl <- rpkmNormLoess$genes$genes[rpkmNormLoess$genes$genes %in% names(hyperVar)]
hyperVarDist <- dist(t(hyperVarRPKM[,1:14]))
hyperVarNJ <- nj(hyperVarDist)

pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_voom.hyper_variable_nj_tree_no_fail.pdf"))
plot(hyperVarNJ, type="unrooted", cex=0.8, tip.color=samplesMeta$indPal, rotate=45)
dev.off()


#########################################################
### 9. CLUSTERING OF SAMPLES BY BRAIN PROMOTER GENES. ###
#########################################################

promotersPal <- brewer.pal(8, "Set3")

promoters <- read.table("../brain_factors.txt", header=F)
promotersEnsembl <- unique(merge(promoters, geneFamilies[,c(1,4)], by.x="V2", by.y="external_gene_name", all.x=T, all.y=F))
promotersEnsembl$col <- promotersPal[promotersEnsembl$V3]
promotersExpr <- data.frame(rpkmNormLoess$E[rpkmNormLoess$genes$genes %in% promotersEnsembl$ensembl_gene_id,])
dim(promotersExpr)
# [1] 31 14
promotersExpr$ensembl <- rpkmNormLoess$genes$genes[rpkmNormLoess$genes$genes %in% promotersEnsembl$ensembl_gene_id]
promotersExpr$external <- promotersEnsembl[promotersEnsembl$ensembl_gene_id %in% promotersExpr$ensembl, 2]
promotersExpr$col <- promotersEnsembl[promotersEnsembl$ensembl_gene_id %in% promotersExpr$ensembl, 5]

pdf(file="eda_plots/brain_factor_expression_hclust_old_no_fail.pdf")
    heatmap.2(as.matrix(promotersExpr[,1:14]), trace="none", main="RPKM, neural promoters", col=rev(colorRampPalette(brewer.pal(9, "RdYlBu"))(30)), labRow=promotersExpr$external, labCol=names(promoters$Expr)[1:14], margins=c(8,4), ColSideColors=paste(samplesMeta$indPal), RowSideColors=promotersExpr$col)
dev.off()

promotersExpr <- promotersExpr[order(promotersExpr$col),]

pdf(file="eda_plots/brain_factor_expression_hclust_2_old_no_fail.pdf")
    heatmap.2(as.matrix(promotersExpr[,1:14]), trace="none", main="RPKM, neural promoters, unclustered", col=rev(colorRampPalette(brewer.pal(9, "RdYlBu"))(30)), labRow=promotersExpr$external, labCol=names(promoters$Expr)[1:14], margins=c(8,4), ColSideColors=paste(samplesMeta$indPal), RowSideColors=promotersExpr$col, Rowv=F, dendrogram="column")

    heatmap.2(as.matrix(promotersExpr[,1:14]), trace="none", main="RPKM, neural promoters, unclustered", col=rev(colorRampPalette(brewer.pal(9, "RdYlBu"))(30)), labRow=promotersExpr$external, labCol=names(promoters$Expr)[1:14], margins=c(8,4), ColSideColors=paste(samplesMeta$indPal), RowSideColors=promotersExpr$col, Colv=F, dendrogram="row")

dev.off()


### Second set of promoters, from Tomoki - this is the better list
factorsPal <- brewer.pal(8, "Set3")

factors <- read.table("../brain_promoters_2.txt", header=F)
factors <- factors[order(factors$V1),]
factors$col <- factorsPal[factors$V3]
factorsExpr <- data.frame(rpkmNormLoess$E[rpkmNormLoess$genes$genes %in% factors$V1,])
dim(factorsExpr)
# [1] 28 14
factorsExpr$ensembl <- rpkmNormLoess$genes$genes[rpkmNormLoess$genes$genes %in% factors$V1]
factorsExpr$external <- factors[factors$V1 %in% factorsExpr$ensembl, 2]
factorsExpr$col <- factors[factors$V1 %in% factorsExpr$ensembl, 4]
factorsExpr <- factorsExpr[order(factorsExpr$col, factorsExpr$external),]


pdf(file="eda_plots/brain_factor_expression_hclust_no_fail.pdf")
    heatmap.2(as.matrix(factorsExpr[,1:14]), trace="none", main="RPKM, neural factors", col=rev(colorRampPalette(brewer.pal(9, "RdYlBu"))(30)), labRow=factorsExpr$external, labCol=names(factors$Expr)[1:14], margins=c(8,4), ColSideColors=paste(samplesMeta$indPal), RowSideColors=factorsExpr$col)
dev.off()


factorsExpr <- factorsExpr[order(factorsExpr$col),]

pdf(file="eda_plots/brain_factor_expression_hclust_2_no_fail.pdf")
    heatmap.2(as.matrix(factorsExpr[,1:14]), trace="none", main="RPKM, neural factors, unclustered", col=rev(colorRampPalette(brewer.pal(9, "RdYlBu"))(30)), labRow=factorsExpr$external, labCol=names(factors$Expr)[1:14], margins=c(8,4), ColSideColors=paste(samplesMeta$indPal), RowSideColors=factorsExpr$col, Rowv=F, dendrogram="column")
dev.off()

pdf(file="eda_plots/brain_factor_expression_hclust_3_no_fail.pdf")
    heatmap.2(as.matrix(factorsExpr[,1:14]), trace="none", main="RPKM, neural factors, unclustered", col=rev(colorRampPalette(brewer.pal(9, "RdYlBu"))(30)), labRow=factorsExpr$external, labCol=names(factors$Expr)[1:14], margins=c(8,4), ColSideColors=paste(samplesMeta$indPal), RowSideColors=factorsExpr$col, Rowv=F, Colv=F, dendrogram="none")
dev.off()



## NJ tree on promoters

promDist <- dist(t(promotersExpr[,1:14]))
promNJ <- nj(promDist)


pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "pc_voom.promoter_nj_trees_no_fail.pdf"))
plot(promNJ, type="unrooted", cex=0.8, tip.color=samplesMeta$indPal, rotate=45)
dev.off()


